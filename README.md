分项目打包规则：

1. package.json中配置启动命令，设置环境变量VITE_PAGE
2. 要打包的项目根目录下至少要有一个main.tsx，用于根元素渲染
3. 在config配置文件中指定VITE_PAGE映射的路径
