import React, { useEffect, useMemo, useState } from "react";
import { Button, Image, Popup, Radio } from "antd-mobile";
import "./index.scss";
import List from "./List";
// import { t } from "@/utils";
import { t } from "@/lang";
import { CloseOutline, RightOutline } from "antd-mobile-icons";
import DiagBtn from "../DiagBtn";
import { staticImg } from "@/config";
import { getAddress } from "@/api/modules/vip_pay";
import { RadioValue } from "antd-mobile/es/components/radio";

const Child: React.FC<{ addrVisible: boolean, onChangeExpress: (address: FreightAddress) => void; }> = ({ addrVisible, onChangeExpress }) => {
    const [visible, setVisible] = useState(false);
    const [visibleAddr, setVisibleAddr] = useState(false);
    const [formData, setFormData] = useState<AddressInfo>();
    const [addrList, setAddrList] = useState<AddressInfo[]>();
    const [freightIndex, setFreightIndex] = useState<RadioValue>(0);
    const [defaultAddress, setDefaultAddress] = useState<AddressInfo>();
    const [addressStatus,setAddressStatus] = useState(true)
    const handleEdit = (item: AddressInfo) => {
        setFormData(item);
        setVisible(true);
    };
    const handleAdd = () => {
        setFormData(undefined);
        setVisible(true);
    };
    const onCloseAddress = async () => {
        const { data } = await getAddress();
        setAddrList(data);
        setVisible(false);
        if (!data?.length) {
            setDefaultAddress(undefined);
        } else {
            
                setDefaultAddress(data?.find(item => item.state));
            
        }
        
    };
    useEffect(() => {
        getAddress().then(res => {
            setDefaultAddress(res.data?.find(item => item.state));
            setAddrList(res.data);
        });
    }, []);

    const onChange = (val: RadioValue) => {
        setFreightIndex(val);
    };
    const onChangeAddressItem = (item: AddressInfo) => {
        setFreightIndex(0);
        setDefaultAddress(item);
        setVisibleAddr(false);

    };
    useEffect(() => {
        console.log("//////////",addrVisible);
        
        if (defaultAddress && addrVisible) {
            console.log("xxxxxxxx");
            
            onChangeExpress({
                shipping_id: defaultAddress.freight[freightIndex as number].id,
                mailing_id: defaultAddress.id,
                mailing_information: [{ email: defaultAddress.email }],
                price: defaultAddress.freight[freightIndex as number].price
            });

        }
    }, [defaultAddress, freightIndex, addrVisible]);
    return (
        <div id="address">
            {defaultAddress ? <div className="default_address">
                <Button block className="address_btn" onClick={() => setVisibleAddr(true)}>
                    <div className="flex-between">
                        <div className="user_info">
                            <p>{defaultAddress.first_name + " " + defaultAddress.last_name}</p>
                            <p>{defaultAddress.phone}</p>
                        </div>
                        <RightOutline color="#8a8a8a" />
                    </div>
                    <div className="address_info mle">{`${defaultAddress.address} ${defaultAddress.province} ${defaultAddress.country}`}</div>
                </Button>
                <Radio.Group value={freightIndex} onChange={onChange}>
                    {defaultAddress.freight?.map((item, index) => <Radio icon={checked => <Image src={`${staticImg}/vip_pay/${checked ? 'checked' : 'unchecked'}.png`} />} value={index} key={item.id}>{item.title}</Radio>)}

                </Radio.Group>
            </div> : <Button block onClick={() => setVisible(true)}><div className="no_address flex-between"><p>{t('vipPay.t_1')}</p><RightOutline color="#8a8a8a" /></div>  </Button>}

            <Popup bodyClassName="address_manage" visible={visibleAddr}>
                <header className="flex-between">
                    <p></p>
                    <p style={{fontSize:"16px", fontWeight:"bold"}}>{t('vipPay.t_2')}</p>
                    <CloseOutline fontSize={16} className="close_icon" onClick={() => setVisibleAddr(false)} />
                </header>
                <main>
                    <ul>
                        {addrList?.map(item => <li key={item.id} onClick={() => onChangeAddressItem(item)} >
                            <div className="flex-between">
                                <div className="left">
                                    <p><span>{item.first_name + item.last_name}</span>{Boolean(item.state) && <span className="default_icon">{t('vipPay.t_3')}</span>} </p>
                                    <p>{item.phone}</p>
                                </div>
                                <div className="right" onClick={() => handleEdit(item)}><Image src={`${staticImg}/vip_pay/edit.png`} /></div>
                            </div>
                            <div className="address_detail mle">{`${item.address} ${item.province} ${item.country}`}</div>
                        </li>)}
                    </ul>
                </main>
                <footer>
                    <DiagBtn onClick={handleAdd}>{t('vipPay.t_4')}</DiagBtn>
                </footer>
            </Popup>
            <List visible={visible} formData={formData} onCloseAddress={onCloseAddress} />
        </div>
    );
};

export default Child;
