import React, { useEffect, useState } from "react";
import { Button, Checkbox, Form, Image, Input, List, Popup, Space, Switch, Toast, reduceMotion } from "antd-mobile";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
import NationList from "./NationList";
import { addAddress, delAddress, updateAddress } from "@/api/modules/vip_pay";
import DiagBtn from "../DiagBtn";

const Child: React.FC<{ visible?: boolean; formData?: AddressInfo, onCloseAddress?: () => void; }> = ({ visible = false, formData, onCloseAddress }) => {
    const [form] = Form.useForm();
    const [nationInfo, setNationInfo] = useState<NationInfo>({});
    const [initialValues, setInitialValues] = useState({ state: 1 });
    useEffect(() => {
        if (visible) {
            form?.resetFields();
            const params = formData ? formData : initialValues;
            form?.setFieldsValue(params);

            console.log(params);
            mergeNationInfo({ country: formData?.country, province: formData?.province, country_id: nationInfo.country_id });
            console.log(form?.getFieldsValue());
        }
        reduceMotion();
    }, [visible]);
    const mergeNationInfo = (payload: any) => {
        const obj = Object.assign({}, nationInfo, payload);
        console.log(obj);
        setNationInfo(obj);
        form.setFieldsValue({ country: obj.country, province: obj.province, country_id: obj.country_id });
        // console.log("ni ",form.getFieldsValue());
        // form.validateFields(["country", "province"])
        
    };
    const onSubmit = () => {
        form?.submit();

    };
    const onFinish = () => {

        let method;
        let type = 0; //0新增，1更新
        if (formData) {
            method = updateAddress;
            type = 1;
        } else {
            method = addAddress;
            type = 0;
        }

        method(form.getFieldsValue()).then(res => {
            if (res.message === "success") {
                Toast.show(type ? t('vipPay.t_5') : t('vipPay.t_6'));
                onCloseAddress?.();
            } else {
                Toast.show(type ? t('vipPay.t_7') : t('vipPay.t_8'));
            }
        });

    }
    const handleDelete = () => {
        delAddress(form.getFieldValue("id")).then(res => {
            if (res.message === "success") {
                Toast.show(t('vipPay.t_9'));
                onCloseAddress?.();
            } else {
                Toast.show(t('vipPay.t_10'));
            }
        });
    };
    const selectState = () => {
        if (nationInfo?.country_id) {

            mergeNationInfo({ visible: true, type: 1, country_id: nationInfo.country_id })
        } else {
            Toast.show(t('vipPay.t_19', { aa: t('vipPay.t_20') }))
        }
    }
    return (
        <Popup visible={visible} bodyClassName="address_list">
            <Form requiredMarkStyle="none" form={form} onFinish={onFinish} mode='card' initialValues={initialValues}
                footer={
                    <div className="flex-between" style={{ gap: "15px" }}>
                        <DiagBtn key={1} dtype="" onClick={onCloseAddress}>{t('vipPay.t_11')}</DiagBtn>
                        {visible && form.getFieldValue("id") && <DiagBtn key={2} dtype="" onClick={handleDelete}>{t('vipPay.t_12')}</DiagBtn>}
                        <DiagBtn key={3} onClick={onSubmit}>{t('vipPay.t_13')}</DiagBtn>
                    </div>
                }>
                <Form.Item name="id" hidden>
                    <Input />
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_15') }) }]} label={t('vipPay.t_15')} name="first_name">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_15') })} />
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_16') }) }]} label={t('vipPay.t_16')} name="last_name">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_16') })} />
                </Form.Item>

                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_17') }) }]} label={t('vipPay.t_17')} name="email">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_17') })} />
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_18') }) }]} label={t('vipPay.t_18')} name="phone">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_18') })} />
                </Form.Item>
                <Form.Header />
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_20') }) }]} label={t('vipPay.t_20')} name="country">
                    <List.Item onClick={() => mergeNationInfo({ visible: true, type: 0 })}>{nationInfo?.country || t('vipPay.t_19', { aa: t('vipPay.t_20') })}</List.Item>
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_21') }) }]} label={t('vipPay.t_21')} name="province">
                    <List.Item onClick={selectState}>{nationInfo?.province || t('vipPay.t_19', { aa: t('vipPay.t_21') })}</List.Item>
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_22') }) }]} label={t('vipPay.t_22')} name="city" >
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_22') })} />
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_23') }) }]} label={t('vipPay.t_23')} name="address">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_23') })} />
                </Form.Item>
                <Form.Item label={t('vipPay.t_24')} name="address_two">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_24') })} />
                </Form.Item>
                <Form.Item rules={[{ required: true, message: t('vipPay.t_53', { aa: t('vipPay.t_25') }) },{max:10,message:t('vipPay.t_54')}]} label={t('vipPay.t_25')} name="zip_code">
                    <Input placeholder={t('vipPay.t_14', { aa: t('vipPay.t_25') })} />
                </Form.Item>
                <Form.Item label={t('vipPay.t_26')} name="state">
                    <Switch  onChange={val =>
                        form?.setFieldValue("state", val)
                    } />
                </Form.Item>
            </Form>
            <NationList visible={nationInfo.visible!} type={nationInfo.type!} country_id={nationInfo.country_id} onNationInfo={mergeNationInfo} />
        </Popup>
    );
};

export default Child;
