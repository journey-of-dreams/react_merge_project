import React, { useEffect, useState } from "react";
import { Button, Checkbox, Form, Image, Input, List, Popup, SearchBar } from "antd-mobile";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
import { CloseOutline, RightOutline } from "antd-mobile-icons";
import { getNationList, getStateList } from "@/api/modules/vip_pay";

const Child: React.FC<NationInfo> = ({ type, visible = false, country_id, onNationInfo }) => {
    const [NationList, setNationList] = useState<NationList[]>();
    const [provinceList, setProvinceList] = useState<string[]>();
    const [keywords, setKeywords] = useState('');
    useEffect(() => {
        if (type) {
            getStateList(country_id!).then(res => {
                console.log(res);
                setProvinceList(res.data);
            });
        } else {
            getNationList().then(res => {
                setNationList(res.data);
            });
        }
    }, [type]);

    useEffect(() => {
        visible && setKeywords("");
    }, [visible])

    const onChange = (val: string) => {
        val = val.trim();
        setKeywords(val);
    }
    return (
        <Popup visible={visible} bodyClassName="nation_list">
            <header className="flex-between">
                <p></p>
                <p>{type ? t('vipPay.t_19', { aa: t('vipPay.t_21') }) : t('vipPay.t_19', { aa: t('vipPay.t_20') })} </p>
                <CloseOutline fontSize={16} className="close_icon" onClick={() => onNationInfo!({ visible: false })} />
            </header>
            <SearchBar onChange={onChange} placeholder={t('vipPay.t_27')} />
            <ul>
                {type ? provinceList?.filter(province => province.includes(keywords)).map((province) => <li key={province} onClick={() => onNationInfo!({ visible: false, province })}>{province}</li>)
                    : NationList?.filter(item => item.country.includes(keywords)).map((item) => <li key={item.id} onClick={() => onNationInfo!({ visible: false, country_id: item.id, country: item.country, province: "" })}>{item.country}</li>)}
            </ul>
        </Popup>
    );
};

export default Child;
