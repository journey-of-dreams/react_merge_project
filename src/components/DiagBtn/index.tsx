import React, { CSSProperties, useEffect, useState } from "react";
import { Button, ButtonProps } from "antd-mobile";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
interface Props extends ButtonProps {
    style?: CSSProperties;
    dtype?: "primary" | "warning" | "danger" |"";
}

const DiagBtn: React.FC<Props> = (props) => {
    const { children, onClick, dtype = "primary", style, ...assets } = props;
    const dstyle = { ...style };
    if (dtype === "primary") {
        Object.assign(dstyle, {
            background: '#9b0008',
            color: '#fff'
        });
    } else {
        Object.assign(dstyle, {
            background: '#fff',
            color: '#000'
        });
    }
    return (
        <div id="diag_btn">
            <Button {...assets} style={dstyle} block onClick={onClick}>{children}</Button>
        </div>
    );
};

export default DiagBtn;
