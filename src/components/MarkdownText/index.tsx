import { message } from 'antd';
import './index.scss';
import 'github-markdown-css'
import ReactMarkdown from 'react-markdown';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import { a11yLight } from 'react-syntax-highlighter/dist/esm/styles/hljs';

a11yLight.hljs.padding = "1em"
console.log(a11yLight);

const codeBlockRenderer = ({ node, inline, className, children, ...props }: any) => {

    const copy = (text: string) => {
        navigator.clipboard?.writeText(text).then(() => {
            message.success("复制成功")
        })
    }
    const match = /language-(\w+)/.exec(className || '')
    return !inline && match ? (
        <>
            <div className='flex-between code_header'><span>{match[1]}</span><span onClick={() => copy(children)} className='copy_btn'>复制</span></div>
            <SyntaxHighlighter
                children={String(children).replace(/\n$/, '')}
                style={a11yLight}
                language={match[1]}
                PreTag="div"
                {...props}
            />
        </>
    ) : (
        <code className={className} {...props}>
            {children}
        </code>
    )
};
export function MarkdownText({ text, className = '', ...props }: { text: string } & React.HTMLAttributes<HTMLDivElement>) {
    return (
        <div className={`markdown-body ${className}`} {...props}>
            <ReactMarkdown components={{ code: codeBlockRenderer }} >
                {text}
            </ReactMarkdown>
        </div>

    );
}