import { FC, useState } from "react";
import "./index.scss";
import { CaretLeftOutlined } from "@ant-design/icons";

interface Props {
  children: React.ReactNode;
  title?: string;
  className?: string;
}
const Sidebar: FC<Props> = (props) => {
  const [isActive, setIsActive] = useState(false);
  return (
    <div id="sidebar">
      <div style={{ height: "100%" }} className={`flex-center ${isActive ? 'active' : 'deactive'}`}>
        <div className={`container ${props.className}`}>
          <header>{props.title}</header>
          {props.children}
        </div>
        <div className="expand_btn" onClick={() => setIsActive(!isActive)}>
          <CaretLeftOutlined />
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
