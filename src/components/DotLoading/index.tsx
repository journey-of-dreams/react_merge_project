import { FC } from "react";
import "./index.scss"
interface Props {
    isShow?: boolean;
}
const DotLoading: FC<Props> = ({ isShow = true }, ref) => {
    return (
        < >
            {isShow && <section className="dots-container">
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
            </section>}


        </>
    );
}

export default DotLoading;
