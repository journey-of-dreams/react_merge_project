/**
 * @description 会员列表项
 */
type MemberListData = {
    id: number,
    name: string,
    is_hot: 0 | 1,
    is_send: number,
    gift_status: number,
    type: 1 | 2 | 3,//一次购买|新用户专享|购买多次
    total_price: number,//原价
    pay_price: number,//支付价
    month: number,
    diag_type: 1 | 2,//diag类型
    goods_sku: string;
};
/**
 * @description 特权列表
 */
type Privileged = {
    ico: string,
    content: string,
    title: string;
};
/**
 * @description 会员列表信息
 */
type MemberList = {
    expiration_time: number,
    time: number,
    is_new_user: boolean,
    is_send_hardware: boolean, //true：非diag或diag2用户
    advertising_figure: string;
    member_list: MemberListData[];
    diag: MemberListData[],
    diag2: MemberListData[];
    privileged: Privileged[];
    new_sale: MemberListData;
};
/**
 * @description 国家下拉列表渲染
 */
type NationList = {
    id: number,
    country: string,
};
/**
 * @description 国家列表信息
 */
type NationInfo = {
    visible?: boolean,
    country_id?: number,
    country?: string,
    province?: string,
    type?: 0 | 1;
    onNationInfo?: (payload: NationInfo) => void;
};
/**
 * @description 地址邮费
 */
type Freight = {
    id: number,
    title: string,
    price: string;
};
/**
 * @description 地址信息
*/
type AddressInfo = {
    id: number,
    first_name: string,
    last_name: string,
    state: 0 | 1,
    phone: string,
    country: string,
    country_id?: number,
    province: string,
    city: string, zip_code: string, address_two: string, email: string,
    address: string,
    freight: Freight[];
};
/**
 * @description 运费与地址信息
*/
type FreightAddress = {
    shipping_id: number,
    mailing_id: number,
    mailing_information: [{ email: string; is_security?: boolean; }];
    price: string;
    original_price?: string;
};
/**
 * @description 订单参数
*/
type OrderParams = {
    goods_price: string,
    shipping: string;
    integral_pay_price: string;
    pay_price: string;
    integral: string;
    goods_list: {
        car_make?: string, car_model?: string; car_year?: string, good_id?: number; good_type: number, number: number; origin_price: string, sku?: string; soft_name?: string; soft_package_id?: string; type: 4; vin?: string;
    }[];
    order_no?: string;
} & FreightAddress;

type DeviceInfo = {
    id: number;
    device_sn: string;
    device_name: string;
    create_time: number;
}