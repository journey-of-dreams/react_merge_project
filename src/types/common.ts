type UglyRef = {
  dom: SVGSVGElement | null;
  generateFace: () => void;
};
type ModalFacebackRef = {
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
};
