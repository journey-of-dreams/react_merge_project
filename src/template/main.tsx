import ReactDOM from "react-dom/client";
import "@/styles/reset.scss";
import "@/styles/common.scss";
import Page from "./index";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);
root.render(<Page />);
