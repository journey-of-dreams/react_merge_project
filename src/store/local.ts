import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";

interface State {
    token: string,
    setToken: (token: string) => void;
}
// persist添加持久化储存
const createLocalStore = persist<State>(
    (set) => ({
        token: "",
        setToken: (token: string) => set({ token }),
    }),
    {
        name: "local",
        storage: createJSONStorage(() => localStorage) //设置存储方式
    }
);
export default create(createLocalStore);