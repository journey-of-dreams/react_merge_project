import { create } from "zustand";
type Info = Record<string, any>;

type RootState = {
    caseStudyForm: Info;
    setCaseStudyForm: (info: Info) => void;
    designCaseStudyForm: (info: Info) => void;
};

const useStore = create<RootState>()(
    (set) => ({
        caseStudyForm: {},
        setCaseStudyForm: (caseStudyForm) => set(() => ({ caseStudyForm: caseStudyForm })),
        designCaseStudyForm: (caseStudyForm: Info) => set((state) => (Object.assign(state.caseStudyForm, caseStudyForm))),
    }),

);
export default useStore;
