import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";

interface State {
    token: string,
    setToken: (token: string) => void;
    cd_code: number;
    setCdcode: (code: number) => void;
}
// persist添加持久化储存
const createSessionStore = persist<State>(
    (set) => ({
        token: "",
        setToken: (token: string) => set({ token }),
        cd_code: -1,
        setCdcode: (cd_code: number) => set({ cd_code }),
    }),
    {
        name: "session",
        storage: createJSONStorage(() => sessionStorage) //设置存储方式
    }
);
export default create(createSessionStore);