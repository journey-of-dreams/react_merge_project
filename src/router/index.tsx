import { lazy } from "react";
import { createBrowserRouter, redirect } from "react-router-dom";
const StudySearch = lazy(() => import("@/apps/case/search"));
const CaseMain = lazy(() => import("@/apps/case/main/index"));
const App = lazy(() => import("../App"));
const Error = lazy(() => import("@/apps/error"));
const Page404 = lazy(() => import("@/apps/error/404"));
const DiagMerge = lazy(() => import("@/apps/report/diag_merge"));
const NewEnergy = lazy(() => import("@/apps/report/new_energy"));
const TireTread = lazy(() => import("@/apps/report/tire_tread"));
const VipPay = lazy(() => import("@/apps/vip_pay/home"));
const VipLogin = lazy(() => import("@/apps/vip_pay/login"));
const VipCode = lazy(() => import("@/apps/vip_pay/vertify_code"));
const VipRegister = lazy(() => import("@/apps/vip_pay/register"));
const SetPwd = lazy(() => import("@/apps/vip_pay/set_pwd"));
const Research = lazy(() => import("@/apps/research/index"));
const Gpt = lazy(() => import("@/apps/gpt/index"));

const routes = [
  {
    project: "case",
    path: "/case",
    element: <App />,
    errorElement: <Error />,
    children: [
      {
        path: "study",
        title: "Case Study",
        element: <CaseMain />,
      },
      {
        path: "search/:type",
        title: "Search",
        element: <StudySearch />,
      },
    ],
  },
  {
    path: "/report",
    element: <App />,
    errorElement: <Error />,
    children: [
      {
        path: "diag_merge",
        title: "Thinkcar",
        element: <DiagMerge />,
      },
      {
        path: "new_energy",
        title: "新能源检测报告",
        element: <NewEnergy />,
      },
      {
        path: "tire_tread",
        title: "胎纹检测报告",
        element: <TireTread />,
      },

    ],
  },
  {
    path: "/vip_pay",
    element: <App />,
    errorElement: <Error />,
    children: [
      {
        path: "home",
        title: "Vip Pay",
        element: <VipPay />,
      },
      {
        path: "login",
        title: "login",
        element: <VipLogin />,
      },
      {
        path: "register",
        title: "register",
        element: <VipRegister />,
      },
      {
        path: "code",
        title: "vertify code",
        element: <VipCode />,
      },
      {
        path: "set_pwd",
        title: "setting password",
        element: <SetPwd />,
      },

    ],
  },
  {
    path: "/research",
    element: <App />,
    errorElement: <Error />,
    children: [{
      path: "index",
      title: "Vip",
      element: <Research />,
    },],
  },
  {
    path: "/gpt",
    element: <App />,
    errorElement: <Error />,
    children: [{
      path: "index",
      title: "gpt",
      element: <Gpt />,
    },],
  },

  {
    path: "*",
    element: <Page404 />,
  },
];


export { routes };
export function registerRoute(project?: string) {
  const findItem = routes.find(item => item.path.slice(1) === project)
  const ru = findItem ? [findItem] : routes;
  return createBrowserRouter(ru)
}
const files = import.meta.glob(["../components/**/*.tsx"], { eager: true });