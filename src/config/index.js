export const staticImg = "https://static.mythinkcar.com/img";

export const pageUrl = {
  case: "./src/apps/case/main.tsx",
  diag_merge: "./src/apps/report/diag_merge/main.tsx",
  new_energy: "./src/apps/report/new_energy/main.tsx",
  tire_tread: "./src/apps/report/tire_tread/main.tsx",
  vip_pay: "./src/apps/vip_pay/main.tsx",
  base: "./src/main.tsx",
  research: "./src/apps/research/main.tsx",
  gpt: "./src/apps/gpt/main.tsx",
};