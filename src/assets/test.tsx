import React, { useEffect, useState } from "react";
import { Image, ProgressCircle } from "antd-mobile";
import { newEnergyReport } from "@/api/modules/report";
import "./index.scss";
import qs from "qs";
import { t } from "@/lang";
const DiagMerge: React.FC = () => {
  const [reportData, setReportData] = useState<any>({});
  const query: Record<string, any> = qs.parse(location.search.slice(1));
  const imgUrl = location.href.includes("https://reportview.thinkcar.cn") ? "https://reportview.thinkcar.cn/report/img/new_energy" : "http://dealer.admin.test.xhinkcar.cn/img/report/new_energy";
  useEffect(() => {
    document.title = t("au.a_8");
    newEnergyReport(query.detectId).then(res => {
      setReportData(res.data);
      console.log(res.data);
    });
  }, []);
  const test = () => {
    console.log(t("au.a_9"), t("au.a_10"));
  };
  return <div id="new_energy">
            <div className="detectNumber" data-name={reportData.detectNumber}>{t("au.a_11")}</div>
    <div className="detectNumber" data-name={reportData.detectNumber}>{t("au.a_12")}</div>
            <div className="core_card" data-name={t("au.a_13")}>
                <div className="serration" data-name={t("au.a_14")}></div>
                <div style={{
        alignItems: "flex-start"
      }} className="flex-start">
                    <Image className="icon" src={`${imgUrl}/report_icon.png`} alt="" />
                    <h1><p>新能源汽车</p><p>电池包快速检测报告</p></h1>
                </div>
               
            </div>
        </div>;
};
export default DiagMerge;