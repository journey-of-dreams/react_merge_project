import ReactDOM from "react-dom/client";
import { registerRoute } from "@/router";
import { RouterProvider } from "react-router-dom";
import "@/styles/reset.scss";
import "@/styles/common.scss";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
  );
root.render(<RouterProvider router={registerRoute()} />);
