import React, { useEffect, useRef, useState } from "react";
import { Checkbox, Form, Image, Input, Swiper, SwiperRef, Tabs } from "antd-mobile";
import "./index.scss";
import { login, getDeviceList } from "@/api/modules/vip_pay";
import qs from "qs";
import LoginLayout from "../component/LoginLayout";
import DiagBtn from "@/components/DiagBtn";
import { useNavigate } from "react-router-dom";
import { t } from "@/lang";
import Agree from "./Agree";
import AgreeConfirm from "./AgreeConfirm";
import useSessionStore from "@/store/session";
const VipPay: React.FC = () => {
  const agreeComRef: any = useRef();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [disabledSubmit, setDisabledSubmit] = useState(true);
  const [isChecked, setIsChecked] = useState(false);
  const {
    cd_code,
    setCdcode
  } = useSessionStore();
  const onFieldsChange = () => {
    setDisabledSubmit(Boolean(form.getFieldError("email").length));
  };
  const submit = () => {
    if (!isChecked) {
      agreeComRef.current?.setVisible(true);
    } else {
      setCdcode(-1);
      navigate("/vip_pay/code", {
        state: {
          login: false,
          email: form.getFieldValue("email")
        }
      });
    }
  };
  return <div id="register">
        <LoginLayout title={t("au.a_22")}>
            <div className="register_title">{t("au.a_23")}</div>
            <Form form={form} onFieldsChange={onFieldsChange} footer={<DiagBtn onClick={submit} disabled={disabledSubmit} block type='submit' color='primary' size='large'>
                {t("au.a_3")}
            </DiagBtn>}>
                <Form.Item name='email' rules={[{
          required: true,
          type: "email",
          message: t("vipPay.t_53", {
            aa: t("au.a_4")
          })
        }]}>
                    <Input clearable placeholder={t("au.a_5")} />
                </Form.Item>
            </Form>
            <Checkbox block checked={isChecked} onChange={value => setIsChecked(value)}><Agree /></Checkbox>
            <div className="login_tip">{t("au.a_24")}<span onClick={() => navigate("/vip_pay/login")}>{t("au.a_25")}</span>
            </div>
            <AgreeConfirm ref={agreeComRef} email={form.getFieldValue("email")} />
        </LoginLayout>

    </div>;
};
export default VipPay;