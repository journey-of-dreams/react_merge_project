import React from "react";
import "./index.scss";
import { t } from "@/lang";
const AGREE_1 = `https://mythinkcar.com/agreement/app/en-us/UserAgreement.html`;
const AGREE_2 = `https://api.mythinkcar.com/home/Thinkdiag/public_policy/lang/en-us/am1.html`;
const AGREE_3 = `https://api.mythinkcar.com/home/Thinkdiag/privacy/lang/en-us/am1.html`;
const AGREE_4 = `https://mythinkcar.com/agreement/app/en-us/am1.html`;
const Com: React.FC = () => <span>{t("au.a_28")}<a href={AGREE_1}>{t("au.a_29")}</a> <a href={AGREE_2}>{t("au.a_30")}</a> <a href={AGREE_3}>{t("au.a_31")}</a> <a href={AGREE_4}>{t("au.a_32")}</a></span>;
export default Com;