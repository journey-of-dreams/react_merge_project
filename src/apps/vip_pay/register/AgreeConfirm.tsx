import React, { forwardRef, useImperativeHandle, useState } from "react";
import "./index.scss";
import qs from "qs";
import { Popup } from "antd-mobile";
import { CloseOutline } from "antd-mobile-icons";
import DiagBtn from "@/components/DiagBtn";
import { useNavigate } from "react-router-dom";
import useSessionStore from "@/store/session";
import Agree from "./Agree";
import { t } from "@/lang";
interface Props {
  email: string;
  ref: any;
}
const Com: React.FC<Props> = forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false);
  const navigate = useNavigate();
  const {
    setCdcode
  } = useSessionStore();
  useImperativeHandle(ref, () => ({
    setVisible
  }));
  const pathJump = () => {
    setCdcode(-1);
    navigate("/vip_pay/code", {
      state: {
        login: false,
        email: props.email
      }
    });
  };
  return <Popup getContainer={null} bodyClassName="confirm_popup" visible={visible}>
        <header className="flex-between">
            <p></p>
            <p>{t("au.a_26")}</p>
            <CloseOutline fontSize={16} className="close_icon" onClick={() => setVisible(false)} />
        </header>
        <main>
            <Agree />
        </main>
        <footer>
            <DiagBtn onClick={pathJump}>{t("au.a_27")}</DiagBtn>
        </footer>
    </Popup>;
});
export default Com;