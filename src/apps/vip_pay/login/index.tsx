import React, { useEffect, useRef, useState } from "react";
import { Form, Input, Swiper, SwiperRef, Tabs } from "antd-mobile";
import "./index.scss";
import { login, getDeviceList } from "@/api/modules/vip_pay";
import qs from "qs";
import LoginLayout from "../component/LoginLayout";
import DiagBtn from "@/components/DiagBtn";
import { useNavigate } from "react-router-dom";
import { t } from "@/lang";
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons";
import useSessionStore from "@/store/session";
const VipPay: React.FC = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [isPsd, setPsd] = useState(true);
  const [showPwd, setShowPwd] = useState(false);
  const {
    cd_code,
    setCdcode
  } = useSessionStore();
  const onFinish = async () => {
    if (isPsd) {
      // 7777@nqmo.com
      // aa123456
      const {
        data
      } = await login({
        ...form.getFieldsValue(),
        verifiable_type: 1
      });
      console.log(data);
      sessionStorage.setItem("token", data.access_token!);
      navigate(`/vip_pay/home?email=${form.getFieldValue("user_name")}&lan=en`);
    } else {
      setCdcode(-1);
      navigate("/vip_pay/code", {
        state: {
          login: true,
          email: form.getFieldValue("user_name")
        }
      });
    }
  };
  return <div id="login">
    <LoginLayout title={t("au.a_1")}>
      <Form form={form} onFinish={onFinish} footer={<DiagBtn block type='submit' color='primary' size='large'>
        {isPsd ? t("au.a_2") : t("au.a_3")}
      </DiagBtn>}>
        <Form.Item name='user_name' rules={[{
          required: true,
          type: "email",
          message: t("vipPay.t_53", {
            aa: t("au.a_4")
          })
        }]}>
          <Input clearable placeholder={t("au.a_5")} />
        </Form.Item>
        {/*  pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*\.()]).{8,20}$/, */}
        {isPsd ? <Form.Item name='password' rules={[{
          required: true,
          message: t("au.a_6")
        }]} extra={showPwd ? <EyeOutline onClick={() => setShowPwd(false)} /> : <EyeInvisibleOutline onClick={() => setShowPwd(true)} />}>
          <Input clearable placeholder={t("au.a_7")} type={showPwd ? 'text' : 'password'} />
        </Form.Item> : null}

      </Form>
      <div className="login_text">{isPsd ? <p onClick={() => setPsd(false)}>{t("au.a_8")}{'>'}</p> : <p onClick={() => setPsd(true)}>{t("au.a_9")}{">"}</p>}</div>
      <div className="no_account_text">{t("au.a_10")} <span onClick={() => navigate("/vip_pay/register")}>{t("au.a_11")}</span></div>
    </LoginLayout>

  </div>;
};
export default VipPay;