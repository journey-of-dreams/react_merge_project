import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { NumberKeyboard, PasscodeInput, Toast } from "antd-mobile";
import "./index.scss";
import { register, login, sendCode, vertifyCode } from "@/api/modules/vip_pay";
import DiagBtn from "@/components/DiagBtn";
import CountDown from "@/components/CountDown";
import { useLocation, useNavigate } from "react-router-dom";
import useSessionStore from "@/store/session";
import { t } from "@/lang";
const VertifyCode: React.FC = () => {
  const countRef: any = useRef();
  const [isFinish, setIsFinish] = useState(true);
  const location = useLocation();
  const [isLogin, setIsLogin] = useState(false);
  const {
    cd_code,
    setCdcode
  } = useSessionStore();
  const navigate = useNavigate();
  useEffect(() => {
    setIsLogin(location.state.login);
    if (cd_code === -1) {
      start();
    }
  }, []);
  const onCodeChange = (val: string) => {
    setIsFinish(val.length == 6);
  };
  const onFill = async (verifiable_code: string) => {
    console.log("完成");
    const type = isLogin ? 1 : 2;
    const params = {
      user_name: location.state.email,
      verifiable_code,
      type
    };
    const {
      code
    } = await vertifyCode(params);
    if (code === 0) {
      if (isLogin) {
        const {
          data
        } = await login({
          user_name: params.user_name,
          password: params.verifiable_code,
          verifiable_type: 2
        });
        sessionStorage.setItem("token", data.access_token!);
        navigate(`/vip_pay/home?email=${params.user_name}&lan=en`, {
          replace: true
        });
      } else {
        const {
          data
        } = await register({
          email: params.user_name,
          verifiable_code: params.verifiable_code
        });
        sessionStorage.setItem("token", data.access_token!);
        navigate(`/vip_pay/set_pwd`, {
          state: {
            email: params.user_name
          },
          replace: true
        });
      }
    }
  };
  const onFinish = () => {
    setIsFinish(true);
  };
  const start = useCallback(() => {
    const email = (location.state.email as string);
    const type = location.state.login ? 1 : 2;
    if (email) {
      sendCode({
        email,
        type
      }).then(res => {
        setCdcode(Date.now() + res.data?.interval * 1000);
        setIsFinish(false);
      });
    } else {
      Toast.show(t("au.a_15"));
    }
  }, []);
  useEffect(() => {
    if (cd_code != -1) {
      countRef.current?.start();
      setIsFinish(cd_code <= Date.now());
    }
  }, [cd_code]);
  return <div id="vertify_code">
            <div className="title">{t("au.a_16")}</div>
            <div className="input_text">{t("au.a_17")}</div>
            <PasscodeInput caret={false} seperated plain keyboard={<NumberKeyboard />} onChange={onCodeChange} onFill={onFill} />
            <DiagBtn disabled={!isFinish} onClick={start}>{t("au.a_18")}<CountDown ref={countRef} onFinish={onFinish} time={cd_code} format="(ss)s" /></DiagBtn>
            {!isLogin && <p className="account_text" onClick={() => navigate("/vip_pay/login")}>{t("au.a_19")}</p>}
        </div>;
};
export default VertifyCode;