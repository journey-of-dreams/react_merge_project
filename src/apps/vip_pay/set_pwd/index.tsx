import React, { useEffect, useRef, useState } from "react";
import { Form, Image, Input, Swiper, SwiperRef, Tabs } from "antd-mobile";
import "./index.scss";
import { edituserInfo } from "@/api/modules/vip_pay";
import qs from "qs";
import LoginLayout from "../component/LoginLayout";
import DiagBtn from "@/components/DiagBtn";
import { useLocation, useNavigate } from "react-router-dom";
import { t } from "@/lang";
import { EyeInvisibleOutline, EyeOutline } from "antd-mobile-icons";
import useSessionStore from "@/store/session";
import { Rule } from "antd-mobile/es/components/form";
const VipPay: React.FC = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [isPsd, setPsd] = useState(true);
  const [showPwd, setShowPwd] = useState(false);
  const [disabledSubmit, setDisabledSubmit] = useState(true);
  const location = useLocation();
  const onFinish = async () => {
    console.log(form.getFieldValue("password"));
    const {
      code
    } = await edituserInfo(form.getFieldValue("password"));
    if (code === 0) {
      navigate(`/vip_pay/home?email=${location.state.email}&lan=en`, {
        replace: true
      });
    }
  };
  const onFieldsChange = (_: any, v: any[]) => {
    setDisabledSubmit(v.some(a => a.errors.length || !a.value));
  };
  const checkMobile = (a: any, value: any) => {
    if (value != form.getFieldValue("password")) {
      return Promise.reject(new Error(t("au.a_20")));
    }
    return Promise.resolve();
  };
  return <div id="set_pwd">
    <div className="tip_text">[The password must be a combination of 8-20 English letters (case sensitive), numbers, and characters
      (only !@#$%^&*.())]</div>
    <Form form={form} onFieldsChange={onFieldsChange} onFinish={onFinish} footer={<DiagBtn disabled={disabledSubmit} block type='submit' color='primary' size='large'>{t("au.a_21")}</DiagBtn>}>
      <Form.Item name='password' rules={[{
        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*\.()]).{8,20}$/,
        message: t("au.a_6")
      }]}>
        <Input clearable placeholder={t("au.a_7")} type="text" />
      </Form.Item>
      <Form.Item name="repeat_pdw" rules={[{
        validator: checkMobile
      }]} extra={showPwd ? <EyeOutline onClick={() => setShowPwd(false)} /> : <EyeInvisibleOutline onClick={() => setShowPwd(true)} />}>
        <Input clearable placeholder={t("au.a_7")} type={showPwd ? 'text' : 'password'} />
      </Form.Item>

    </Form>

  </div>;
};
export default VipPay;