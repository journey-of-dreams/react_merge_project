import React, { useEffect, useState } from "react";
import { Image } from "antd-mobile";
import { staticImg } from "@/config";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
import qs from "qs";
import { formatDate } from "@/utils";
const query: Record<string, any> = qs.parse(location.search.slice(1));
interface Props {
  expiration_time?: number;
  time?: number;
}
const Child: React.FC<Props> = ({
  expiration_time,
  time
}) => {
  const reset_time = expiration_time! * 1000 - new Date().getTime();
  console.log(reset_time, `过期日期：${formatDate(expiration_time)}`);
  const statusMessage = () => {
    if (time) {
      if (expiration_time === 0) {
        return `${t('vipPay.t_42')}\n${t('vipPay.t_43')}`;
      } else if (expiration_time! < time!) {
        return `${t('vipPay.t_44')}\n${t('vipPay.t_45', {
          aa: (formatDate(expiration_time!) as string)
        })}`;
      } else {
        return `${t('vipPay.t_46')}\n${t('vipPay.t_47', {
          aa: (formatDate(expiration_time!) as string)
        })}`;
      }
    }
  };
  return <div id="vap_card">
            <div className="left">
                <Image className="text" src={`${staticImg}/vip_pay/vip_text.png`} />
                <p style={{
        whiteSpace: "pre-line"
      }}>{statusMessage()}</p>
            </div>
            <Image className="medal" src={`${staticImg}/vip_pay/medal.png`} />
        </div>;
};
export default Child;