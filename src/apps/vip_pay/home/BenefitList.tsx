import React, { useEffect, useState } from "react";
import { Image } from "antd-mobile";
import { staticImg } from "@/config";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
const Child: React.FC<{
  list?: Privileged[];
}> = ({
  list
}) => {
  return <div className="behefit_list">
            <div className="title">{t('vipPay.t_28')}</div>
            <ul className="privilege_list">
                {list?.map((item, index) => <li key={item.title} className="flex-start">
                        <Image width={40} src={`${staticImg}/vip_pay/icon${index + 1}.png`} />
                        <div className="right">
                            <p className="title">{item.title}</p>
                            <p className="content">{item.content}</p>
                        </div>
                    </li>)}
            </ul>
        </div>;
};
export default Child;