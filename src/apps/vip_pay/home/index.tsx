import React, { useEffect, useMemo, useRef, useState } from "react";
import { Image, List, Swiper, SwiperRef, Tabs } from "antd-mobile";
import { staticImg } from "@/config";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
import { getBuyMemberList, getDeviceList } from "@/api/modules/vip_pay";
import qs from "qs";
import VipCard from "./VipCard";
import VipList from "./VipList";
import BenefitList from "./BenefitList";
import Pay from "./Pay";
import mittBus from "@/utils/mittBus";
import DeviceList from "./DeviceList";
const query: Record<string, any> = qs.parse(location.search.slice(1));
const {
  lan,
  sn,
  serialNo
} = query;
const VipPay: React.FC = () => {
  const [newSn, setNewSn] = useState<string>(sn || serialNo);
  const swiperRef = useRef<SwiperRef>(null);
  const [activeIndex, setActiveIndex] = useState(0);
  const [memberInfo, setMemberInfo] = useState<MemberList>();
  const [deviceList, setDeviceList] = useState<DeviceInfo[]>();
  let lock = false;
  useEffect(() => {
    getDeviceList().then(res => {
      setDeviceList(res.data.list);
    });
  }, []);
  useEffect(() => {
    if (deviceList) {
      const newSn = serialNo || sn || deviceList[0]?.device_sn;
      onChangeSn(newSn);
    }
  }, [deviceList]);
  useEffect(() => {
    memberInfo && mittBus.emit("goodsItem", memberInfo.diag![0]);
  }, [memberInfo]);
  const onChangeSn = (sn: string) => {
    !newSn && setNewSn(sn);
    getBuyMemberList(lan, sn).then(res => {
      res.data.diag = newSn && !res.data?.is_send_hardware ? res.data?.member_list : res.data.member_list.filter(item => item.diag_type === 1);
      res.data.diag2 = res.data.member_list.filter(item => item.diag_type === 2);
      setMemberInfo({
        ...res.data,
        time: res.time
      });
    });
  };
  const onTabChange = (key: string) => {
    if (key == "1" && !lock) {
      mittBus.emit("goodsItem", memberInfo?.diag2![0]);
      lock = true;
    }
    setActiveIndex(Number(key));
    swiperRef.current?.swipeTo(Number(key));
  };
  return <div id="home">
            <header>
                <VipCard expiration_time={memberInfo?.expiration_time} time={memberInfo?.time} />
            </header>
            <main>
                {memberInfo?.advertising_figure && <Image className="advertise" src={memberInfo?.advertising_figure} />}
                {deviceList?.length ? <DeviceList deviceList={deviceList} onChangeSn={onChangeSn} /> : null}
                {(!newSn || memberInfo?.is_send_hardware) && <Tabs activeKey={"" + activeIndex} activeLineMode="full" onChange={onTabChange}>
                    <Tabs.Tab title='THINKDIAG' key={0}>
                    </Tabs.Tab>
                    <Tabs.Tab title='THINKDIAG2' key={1}>
                    </Tabs.Tab>

                </Tabs>}
                <Swiper direction='horizontal' loop indicator={() => null} ref={swiperRef} defaultIndex={activeIndex} onIndexChange={index => setActiveIndex(index)} allowTouchMove={!newSn || memberInfo?.is_send_hardware}>
                    <Swiper.Item>
                        <VipList vkey={0} activeIndex={activeIndex} diagData={{
            diag: memberInfo?.diag,
            diag2: memberInfo?.diag2
          }} />
                    </Swiper.Item>
                    <Swiper.Item>
                        <VipList vkey={1} activeIndex={activeIndex} diagData={{
            diag: memberInfo?.diag,
            diag2: memberInfo?.diag2
          }} />
                    </Swiper.Item>
                </Swiper>
                {memberInfo?.is_send_hardware && <div className="tip">
                    {t('vipPay.t_29')}
                </div>}
                <BenefitList list={memberInfo?.privileged} />
            </main>
            <footer>
                {memberInfo && <Pay is_send_hardware={memberInfo?.is_send_hardware} is_new_user={memberInfo?.is_new_user} new_sale={memberInfo.new_sale} />}
            </footer>

        </div>;
};
export default VipPay;