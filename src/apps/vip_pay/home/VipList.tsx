import React, { useEffect, useState } from "react";
import "./index.scss";
import { t } from "@/lang";
import mittBus from "@/utils/mittBus";
const Child: React.FC<{
  vkey: number;
  activeIndex: number;
  diagData: {
    diag?: MemberListData[];
    diag2?: MemberListData[];
  };
}> = ({
  vkey,
  activeIndex,
  diagData
}) => {
  const [curIndex, setCurIndex] = useState<number>(0);
  const [list, setList] = useState<MemberListData[]>();
  const label = (type: number, is_hot: 0 | 1) => {
    if (type === 1) {
      return t('vipPay.t_48');
    } else if (type === 2) {
      return t('vipPay.t_49');
    } else if (is_hot === 1) {
      return t('vipPay.t_50');
    } else {
      return "";
    }
  };
  const onSelect = (index: number, item: MemberListData) => {
    setCurIndex(index);
  };
  useEffect(() => {
    if (diagData.diag && diagData.diag2) {
      if (activeIndex && vkey == activeIndex) {
        setList(diagData.diag2);
        mittBus.emit("goodsItem", diagData.diag2[curIndex]);
      } else if (vkey == activeIndex) {
        setList(diagData.diag);
        mittBus.emit("goodsItem", diagData.diag[curIndex]);
      }
    }
  }, [activeIndex, diagData, curIndex]);
  return <div className="flex-start card_wrap">
            {list?.map((item, index) => <div onClick={() => onSelect(index, item)} className={`normal ${index === curIndex ? "selected" : "unselected"}`} key={item.id}>
                    <p className="label">{label(item.type, item.is_hot)}</p>
                    <p className="title">{item.name}</p>
                    <div className="price"><span>$</span><span>{item.pay_price}</span></div>
                    <div className="account_price">{curIndex === index && item.pay_price < item.total_price ? <p className="original_account">{t('vipPay.t_51')}${item.total_price}</p> : <p>{t('vipPay.t_52', {
            aa: (item.pay_price / item.month).toFixed(2)
          })}</p>} </div>
                </div>)}
        </div>;
};
export default Child;