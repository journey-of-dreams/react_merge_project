import ReactDOM from "react-dom/client";
import "@/styles/reset.scss";
import "@/styles/common.scss";
import VipPay from "./home/index";
import qs from "qs";
import { langTransform } from "@/utils";
import { RouterProvider } from "react-router-dom";
import { registerRoute } from "@/router";
const query: Record<string, any> = qs.parse(location.search.slice(1));
langTransform(query.lan);
const root = ReactDOM.createRoot((document.getElementById("root") as HTMLElement));

root.render(<RouterProvider router={registerRoute("vip_pay")} />);