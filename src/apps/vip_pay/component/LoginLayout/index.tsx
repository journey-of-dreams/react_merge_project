import React from "react";
import { Button, ButtonProps, Image } from "antd-mobile";
import "./index.scss";
import { staticImg } from "@/config";

interface Props {
  title: string;
  children: React.ReactNode;
}
const LoginLayout: React.FC<Props> = props => {
  return <div id="login_layout">
    <div className="bg">
      <Image src={`${staticImg}/vip_pay/login_bg.png`} />
      <span>{props.title}</span>
    </div>
    <div className="float_lay">
      {props.children}
    </div>
  </div>;
};
export default LoginLayout;