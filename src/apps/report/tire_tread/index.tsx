import React, { useEffect, useState } from "react";
import { Collapse, Divider, Image, ProgressCircle } from "antd-mobile";
import { newEnergyReport } from "@/api/modules/report";
import "./index.scss";
import qs from "qs";
import { DownOutline, InformationCircleOutline, UpOutline } from "antd-mobile-icons";

const TireTread: React.FC = () => {
    const [reportData, setReportData] = useState<any>({});

    const query: Record<string, any> = qs.parse(location.search.slice(1));
    const imgUrl = location.href.includes("https://reportview.thinkcar.cn") ? "https://reportview.thinkcar.cn/report/img/tire_tread" : "http://dealer.admin.test.xhinkcar.cn/img/report/tire_tread";
    useEffect(() => {
        newEnergyReport(query.detectId).then(res => {
            setReportData(res.data);
            console.log(res.data);
        })
    }, [])

    return (
        <div id="tire_tread">
            <div className="heading">
                <div className="title">胎温监测</div>
                <div className="heading_info">
                    <p className="first">SN:<span>123455676554</span></p>
                    <p>2024/08/02 12:23:43</p>
                </div>
            </div>
            <div className="card client_info">
                <Collapse arrow={active => active ? <div><span>收起</span><DownOutline /></div> : <div><span>展开</span><UpOutline /></div>}>
                    <Collapse.Panel key='1' title='客户名称客户名称'>

                        <div className="flex-start client_content">
                            <div className="client_item">
                                <div className="title">车牌号</div>
                                <div className="content">粤B242322</div>
                            </div>
                            <div className="client_item">
                                <div className="title">VIN码</div>
                                <div className="content">3783GSJFDEO87894933</div>
                            </div>
                            <div className="client_item">
                                <div className="title">电话</div>
                                <div className="content">12374970383</div>
                            </div>
                        </div>
                    </Collapse.Panel>
                </Collapse>
            </div>

            <div className="diag_result">
                <div className="flex-between">
                    <span>检测结果</span>
                    <span className="num">3</span>
                </div>
                <ul className="card content">
                    <li>1.左前轮、右前轮：磨损严重建议立即更换</li>
                    <li>2.左前轮、右前轮：磨损严重建议立即更换</li>
                </ul>
            </div>
            <div className="diag_record">
                <Collapse>
                    <Collapse.Panel key='1' title={<div>
                        <span>检测记录</span>
                        <div className="title_content"><span>异常轮胎</span><span className="num red">3</span></div>
                    </div>}>

                        <div className="card content">
                            <div className="title flex-between">
                                <div className="flex-start"><Image src={`${imgUrl}/tr_lt.png`} /><span>左前轮</span></div>
                                <div className="num red">1</div>
                            </div>
                            <ul>
                                <li className="flex-between">
                                    <div>凹槽1</div>
                                    <div>8.8 mm</div>
                                    <div>磨损严重</div>
                                </li>
                            </ul>
                        </div>
                    </Collapse.Panel>
                </Collapse>


            </div>
            <div className="diag_record">
                <Collapse>
                    <Collapse.Panel key='1' title={<div>
                        <span>检测记录</span>
                        <div className="flex-start"><div className="title_content"><span>故障码</span><span className="num red">3</span></div> <div className="title_content"><span>异常系统</span><span className="num red">3</span></div></div>
                    </div>}>

                        <div className="card content">
                            <Collapse>
                                <Collapse.Panel key='1' title="PCM (Powertrain Control Module) ">
                                    <ul className="card_child">
                                        <li className="flex-start">
                                            <div className="red">P29393</div>
                                            <div>EGR Valve A flow Insufficient DetectedEGR Valve A flow Insufficient Detected</div>
                                        </li>
                                        <li className="flex-start">
                                            <div className="red">P29393</div>
                                            <div>EGR Valve A flow Insufficient DetectedEGR Valve A flow Insufficient Detected</div>
                                        </li>
                                    </ul>
                                </Collapse.Panel></Collapse>

                        </div>
                    </Collapse.Panel>
                </Collapse>
            </div>

            <div className="diag_record fauld_record">
                <Collapse>
                    <Collapse.Panel key='1' title="检测记录">
                        <div className="card content">
                            <p>润滑油检测</p>
                            <p className="flex-start diag_status">
                                <InformationCircleOutline color="#ff5c26" />
                                <span className="red">Light transmittance qualified</span>
                            </p>
                            <ul className="card_child">
                                <li className="flex-between"><div>Transmittance</div><div className="num" style={{color:"#fff"}}>89%</div></li>
                            </ul>
                        </div>
                    </Collapse.Panel>
                </Collapse>
            </div>
        </div>
    );
};

export default TireTread;
