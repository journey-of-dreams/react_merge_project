import React, { useEffect, useState } from "react";
import { Image, ProgressCircle } from "antd-mobile";
import {  newEnergyReport } from "@/api/modules/report";
import "./index.scss";
import qs from "qs";

const DiagMerge: React.FC = () => {
    const [reportData, setReportData] = useState<any>({});
    const query: Record<string, any> = qs.parse(location.search.slice(1));
    const imgUrl = location.href.includes("https://reportview.thinkcar.cn") ? "https://reportview.thinkcar.cn/report/img/new_energy" : "http://dealer.admin.test.xhinkcar.cn/img/report/new_energy";
    useEffect(() => {
        document.title="固恒新能源";
        newEnergyReport(query.detectId).then(res => {
            setReportData(res.data);
            console.log(res.data);
        })
    }, [])

    return (
        <div id="new_energy">
            <div className="detectNumber">{reportData.detectNumber}</div>
            <div className="core_card">
                <div className="serration"></div>
                <div style={{ alignItems: "flex-start" }} className="flex-start">
                    <Image className="icon" src={`${imgUrl}/report_icon.png`} alt="" />
                    <h1><p>新能源汽车</p><p>电池包快速检测报告</p></h1>
                </div>
                <div className="part_1">
                    <h2>检测机构</h2>
                    <div className="item">
                        <div className="title">修理厂名称</div>
                        <div className="content">{reportData.garageName}</div>
                    </div>
                    <div className="item">
                        <div className="title">地址</div>
                        <div className="content">{reportData.garageAddress}</div>
                    </div>
                    <div className="item">
                        <div className="title">电话</div>
                        <div className="content">{reportData.garageTel}</div>
                    </div>
                    <div className="item">
                        <div className="title">FC01序列号</div>
                        <div className="content">{reportData.fc01Sn}</div>
                    </div>
                    <div className="item">
                        <div className="title">检测时间</div>
                        <div className="content">{reportData.createdAt}</div>
                    </div>
                </div>
                <div className="serration"></div>
                <div className="part_2">
                    <h2>检测结果</h2>
                    <div className="flex-between" style={{ alignItems: "flex-start" }}>
                        <div className="item">
                            <div><div className="title">额定容量</div>
                                <div className="content">{reportData.nominalCapacity}Ah</div></div>
                            <div>
                                <div className="title">额定电压</div>
                                <div className="content">{reportData.ratedTotalVoltage}V</div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="soc_circle_wrap">

                                <ProgressCircle className="soc_circle" percent={Number(reportData.odata?.vehiclePowerBatteryChargeState)}>
                                    <span>{reportData.odata?.vehiclePowerBatteryChargeState}%</span>
                                </ProgressCircle>
                            </div>
                            <p className="soc_text">SOC</p>
                        </div>
                    </div>
                    <div className="flex-between">
                        <div className="item">
                            <div className="title">当前电压</div>
                            <div className="content">{reportData.vehiclePowerBatteryCurrentVoltage}V</div>
                        </div>
                        <div className="item">
                            <div className="title">电池组充电次数</div>
                            <div className="content">{reportData.odata?.batteryChargeNumber}次</div>
                        </div>
                    </div>
                    <div className="flex-between">
                        <div className="item">
                            <div className="title">电池生产商</div>
                            <div className="content">{reportData.batteryManufacturer}</div>
                        </div>
                        <div className="item">
                            <div className="title">电池类型</div>
                            <div className="content">{reportData.batteryType}</div>
                        </div>
                    </div>
                    <div className="flex-between"> <div className="item">
                        <div className="title">标称能量</div>
                        <div className="content">kW.h</div>
                    </div>
                        <div className="item">
                            <div className="title">电池组生产日期</div>
                            <div className="content">{reportData.odata?.batteryProductionDate}</div>
                        </div></div>






                    <div className="item">
                        <div className="title">VIN</div>
                        <div className="content">{reportData.vin}</div>
                    </div>

                </div>
                {reportData.odata && <div className="part_3">
                    <div className="blue">BMS数据</div>
                    <div className="table">
                        <div className="table_header flex-between">
                            <div>名称</div><div>值</div><div>单位</div>
                        </div>
                        <div className="table_row">
                            <div>最高允许充电总电压</div>
                            <div>{reportData.odata.maxAllowChargeVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>BMS通信协议版本号</div>
                            <div>{reportData.odata.bmsProtocolVersion}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>单体动力蓄电池最高允许充电电压	</div>
                            <div>{reportData.odata.maxAllowSingleChargeVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>最高允许充电电流</div>
                            <div>{reportData.odata.maxAllowChargeCurrent}</div>
                            <div>A</div>
                        </div>
                        <div className="table_row">
                            <div>最高允许温度</div>
                            <div>{reportData.odata.maxAllowTemperature}</div>
                            <div>℃</div>
                        </div>
                        <div className="table_row">
                            <div>整车动力蓄电池荷电状态</div>
                            <div>{reportData.odata.vehiclePowerBatteryChargeState}</div>
                            <div>%</div>
                        </div>
                        <div className="table_row">
                            <div>充电电压测量值</div>
                            <div>{reportData.odata.chargevoltageMeasurement}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>充电电流测量值</div>
                            <div>{reportData.odata.chargeCurrentMeasurement}</div>
                            <div>A</div>
                        </div>
                        <div className="table_row">
                            <div>辨识结果</div>
                            <div>{reportData.pushResult}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>电池类型</div>
                            <div>{reportData.garageType}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>额定容量</div>
                            <div>{reportData.odata.nominalCapacity}</div>
                            <div>Ah</div>
                        </div>
                        <div className="table_row">
                            <div>额定总电压</div>
                            <div>{reportData.odata.ratedTotalVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>电池生产厂商名称</div>
                            <div>{reportData.odata.batteryManufacturer}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>电池组序号</div>
                            <div>{reportData.odata.batteryNumber}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>电池组生产日期</div>
                            <div>{reportData.odata.batteryProductionDate}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>电池组充电次数</div>
                            <div>{reportData.odata.batteryChargeNumber}</div>
                            <div>次</div>
                        </div>
                        <div className="table_row">
                            <div>电池组产权标识</div>
                            <div>{reportData.odata.batteryLabel}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>车辆识别码</div>
                            <div>{reportData.vin}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>当前BMS版本信息</div>
                            <div>{reportData.odata.bmsProtocolVersion}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池标称总能量</div>
                            <div>{reportData.totalEnergy}</div>
                            <div>KW.h</div>
                        </div>
                        <div className="table_row">
                            <div>整车动力蓄电池当前电池电压</div>
                            <div>{reportData.odata.vehiclePowerBatteryCurrentVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>电压需求</div>
                            <div>{reportData.odata.voltageReq}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>电流需求</div>
                            <div>{reportData.odata.currentreq}</div>
                            <div>A</div>
                        </div>
                        <div className="table_row">
                            <div>充电模式</div>
                            <div>{reportData.odata.chargemode}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>最高单体动力蓄电池电压</div>
                            <div>{reportData.odata.maxSingleChargeVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>最高单体动力组号</div>
                            <div>{reportData.odata.maxPowerGroupNum}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>当前荷电状态SOC</div>
                            <div>{reportData.odata.currentSoc}</div>
                            <div>%</div>
                        </div>
                        <div className="table_row">
                            <div>最高单体动力蓄电池电压所在编号</div>
                            <div>{reportData.odata.maxSingleChargeVoltageNumber}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>最高动力蓄电池温度</div>
                            <div>{reportData.odata.maxPowerBatteryTemperature}</div>
                            <div>℃</div>
                        </div>
                        <div className="table_row">
                            <div>最高温度检测点编号</div>
                            <div>{reportData.odata.maxDetectNumber}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>最低动力蓄电池温度</div>
                            <div>{reportData.odata.minPowerBatteryTemperature}</div>
                            <div>℃</div>
                        </div>
                        <div className="table_row">
                            <div>最低动力蓄电池温度检测点编号</div>
                            <div>{reportData.odata.minPowerBatteryTemperature_number}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>单体动力蓄电池状态</div>
                            <div>{reportData.odata.singleBatteryStatus}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>整车动力蓄电池荷电状态SOC状态</div>
                            <div>{reportData.odata.socState}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池充电过流状态</div>
                            <div>{reportData.odata.batteryChargePointState}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池温度状态</div>
                            <div>{reportData.odata.batteryTemperatureStatus}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池绝缘状态</div>
                            <div>{reportData.odata.insulationStatus}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池组输出连接器连接状态</div>
                            <div>{reportData.odata.connecttionStatus}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>充电允许</div>
                            <div>{reportData.odata.allowCharge}</div>
                            <div></div>
                        </div>
                        <div className="table_row">
                            <div>中止荷电状态SOC</div>
                            <div>{reportData.odata.abortSocChargeState}</div>
                            <div>℃</div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池单体最低电压</div>
                            <div>{reportData.odata.lowBatteryVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池单体最高电压</div>
                            <div>{reportData.odata.maxBatteryVoltage}</div>
                            <div>V</div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池最低温度</div>
                            <div>{reportData.odata.minBatteryBemperature}</div>
                            <div>℃</div>
                        </div>
                        <div className="table_row">
                            <div>动力蓄电池最高温度</div>
                            <div>{reportData.odata.maxBatteryTemperature}</div>
                            <div>℃</div>
                        </div>
                    </div>
                </div>}


            </div>
        </div>
    );
};

export default DiagMerge;
