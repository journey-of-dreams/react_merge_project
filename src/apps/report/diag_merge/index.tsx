import React, { useEffect, useState } from "react";
import { Image } from "antd-mobile";
import { staticImg } from "@/config";
import { diagMergeReport } from "@/api/modules/report";
import "./index.scss";
// import { t } from "@/utils";
import { t } from "@/lang";
import qs from "qs";

type FaultStatus = {
    title: string;
    context: string;
};
type ModuleStatus = {
    name: string;
    value: string;
};
type ModuleInfo = {
    name: string;
    value: ModuleStatus[];
};
const DiagMerge: React.FC = () => {
    const [reportData, setReportData] = useState<any>({});
    const query: Record<string, any> = qs.parse(location.search.slice(1));
    console.log('====================location.search', location.search, '=========', location, '*********', query);
    useEffect(() => {

        try {
            // @ts-ignore
            const data = appContactUsObject.getDiagMergeData();
            disposeData(data);
        } catch (error) {
            const id = query.id;

            if (!id) return;
            diagMergeReport(id).then(res => {
                disposeData(res.data);
            });
        }
    }, []);
    const disposeData = (data: any) => {
        console.log(data);
        let maxVilige: ModuleStatus[] = [];
        data.module_info.forEach((module_item: ModuleInfo) => {
            if (module_item.value.length > maxVilige.length) {
                maxVilige = module_item.value;
            }
        });
        data.maxEmpty = maxVilige;
        setReportData(data);
    };
    return (
        <div id="diag_merge">
            <header className="body_header">
                <Image src={`${staticImg}/report/diag_bg.png`} />
                <div className="title">{t("report.t_1")}</div>
            </header>
            <div className="body">
                <div className="body_part">
                    <div className="title">{t("report.t_2")}123456</div>
                    <ul className="flex-start">
                        <li>
                            <div className="name">{t("report.t_3")}</div>
                            <div className="content">{reportData.shop_info?.name}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_4")}</div>
                            <div className="content">{reportData.shop_info?.mobile}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_5")}</div>
                            <div className="content">{reportData.shop_info?.serial_number}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_6")}</div>
                            <div className="content">{reportData.shop_info?.datetime}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_7")}</div>
                            <div className="content">{reportData.shop_info?.addr}</div>
                        </li>
                    </ul>
                    <Image className="waterplink" src={`${staticImg}/report/mark.png`} />
                </div>
                <div className="body_part2">
                    <div className="title">{t("report.t_8")}</div>
                    <ul className="flex-start">
                        <li>
                            <div className="name">{t("report.t_9")}</div>
                            <div className="content">{reportData.car_info?.voltage}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_10")}</div>
                            <div className="content">{reportData.car_info?.software_version}</div>
                        </li>
                        <li>
                            <div className="name">{t("report.t_11")}</div>
                            <div className="content">{reportData.car_info?.current}</div>
                        </li>
                        <li>
                            <div className="name">VIN</div>
                            <div className="content">{reportData.car_info?.vin}</div>
                        </li>
                        <li>
                            <div className="name">SOC</div>
                            <div className="content">{reportData.car_info?.soc}</div>
                        </li>
                    </ul>
                    <Image className="waterplink" src={`${staticImg}/report/seal.png`} />
                </div>
                <div className="body_part3">
                    <header className="flex-between">
                        <div className="title">{t("report.t_12")}</div>
                        <div className="subtitle" dangerouslySetInnerHTML={{ __html: t(t("report.t_13"), { aa: "<span>5</span>" }) }}></div>
                    </header>
                    <ul>
                        {reportData.fault_status?.map((item: FaultStatus) => <li key={item.title} className="flex-between"><span className="title">{item.title}</span><span className="context">{item.context}</span></li>)}
                    </ul>
                </div>
                <div className="body_part4">
                    <div className="body_title">{t("report.t_14")}</div>
                    <ul>
                        {reportData.module_status?.map((item: ModuleStatus) => <li key={item.name} className="flex-between"><span className="title">{item.name}</span><span className="context">{item.value}</span></li>)}
                    </ul>
                </div>
                <div className="body_part5">
                    <div className="title">{t("report.t_15")}</div>
                    <div className="table">
                        <div className="table_column">
                            <div>{t("report.t_17")}</div>
                            {reportData.maxEmpty?.map((item: ModuleStatus) => <div key={item.name}>{item.name} </div>)}
                        </div>
                        {reportData.module_info?.map((item: ModuleInfo, index: number) => <div className="table_column" key={item.name}>
                            <div>{item.name}</div>
                            {reportData.maxEmpty?.map((_: ModuleInfo, index: number) => <div key={index}>{item.value[index]?.value}</div>)}
                        </div>)}

                    </div>

                </div>
            </div>
        </div>
    );
};

export default DiagMerge;
