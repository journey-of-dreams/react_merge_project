import ReactDOM from "react-dom/client";
import "@/styles/reset.scss";
import "@/styles/common.scss";
import {registerRoute} from "@/router";
import { RouterProvider } from "react-router-dom";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);
root.render(<RouterProvider router={registerRoute("case")} />);
