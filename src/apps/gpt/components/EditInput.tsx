import { Button, Input } from "antd";
import { DownOutlined, EditOutlined } from "@ant-design/icons";
import React, { FC, forwardRef, useEffect, useImperativeHandle, memo, useState } from "react";

interface Props {
    content: string;
    onEditMsg?: (content: string) => void;
}
const PageChild: FC<Props> = (props, ref) => {
    const [isEdit, setIsEdit] = useState(false)
    const [value, setValue] = useState("")

    const startEdit = () => {
        setValue(props.content)
        setIsEdit(true)
    }
    const onSubmit = () => {
        if (value) {
            console.log("提交 ");
            setIsEdit(false)
            props.onEditMsg?.(value)
        }
    }
    return (
        < >
            {isEdit ? <div className="flex-end edit_wrap">
                <Input.TextArea style={{width:"300px"}} autoFocus value={value} onChange={e => setValue(e.target.value)} onPressEnter={onSubmit} />
                <Button onClick={() => setIsEdit(false)}>取消</Button>
                <Button type="primary" onClick={onSubmit}>确定</Button></div>
                : <div  className="tool_item" onClick={startEdit}><EditOutlined /><span>编辑</span></div>}


        </>
    );
}

export default PageChild;
