import React, { forwardRef, useEffect, useImperativeHandle, useMemo, useRef, useState } from "react";
import { Button, Drawer, FloatButton, Form, Input, InputRef, List, message, Modal, Popover, Tooltip } from "antd";
import UglyAvatar from "@/components/UglyAvatar";
import { DeleteFilled, EditOutlined, ExclamationCircleFilled, PlusOutlined, RedoOutlined } from "@ant-design/icons";
import { deleteDialog, editDialog, getDialogList, updateUserInfo } from "@/api/modules/gpt";
import { Gpt } from "@/api/interface";
import type { SearchProps } from 'antd/es/input/Search';
import ModalFaceback from "./ModalFaceback";
const userDatas = ["月之两面", "用户反馈", "退出登录"]

interface Props {
    userInfo: Gpt.UserInfo,
    ref: any;
    onDialogOpt?: (_userInfo: Gpt.DialogItem) => void;
    onCreateDialog?: () => Promise<void>
}

const PageChild: React.FC<Props> = forwardRef((props, ref) => {

    const [drawerOpen, setDrawerOpen] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const [avatarLoading, setAvatarLoading] = useState(false)
    const [chatList, setChatList] = useState<Gpt.DialogList>()
    const uglyRef = useRef<UglyRef>(null);
    const titleRef = useRef<InputRef>(null);
    const modalFacebackRef = useRef<ModalFacebackRef>(null);
    const [form] = Form.useForm();
    const [activeDialogId, setActiveDialogId] = useState("")

    useImperativeHandle(ref, () => ({
        chatList,
        setChatList,
        setDrawerOpen
    }))
    useEffect(() => {
        // last_dialog_id改变时自动获取回话列表
        if (props.userInfo.last_dialog_id) {
            if (activeDialogId === props.userInfo.last_dialog_id) {
                return
            }

            getDialogList().then(res => {
                setActiveDialogId(props.userInfo.last_dialog_id)
                setChatList(() => res.data)
                setTimeout(async () => {
                    const activeItem = res.data.list.find(item => item._id === props.userInfo.last_dialog_id)
                    if (activeItem) {
                        props.onDialogOpt?.(activeItem!)
                    } else {
                        await props.onCreateDialog?.()
                        message.success("检测到该会话不存在，已为您创建新会话")
                    }
                });
            })
        }
    }, [props.userInfo.last_dialog_id])


    // 会话操作
    // 会话表单提交
    const onDialogFinish = async () => {
        console.log(form.getFieldsValue());
        setModalOpen(false)
        message.success("会话编辑成功");


    }

    return (
        < >
            <div className="plusIntelige">
                <Tooltip title="添加我的智能体">
                    <Button shape="circle" icon={<PlusOutlined />} />
                </Tooltip>
            </div>
            <Modal title="创建智能体" open={modalOpen} onCancel={() => setModalOpen(false)} onOk={() => form.submit()} okText="发布">
                <Form
                    form={form}
                    onFinish={onDialogFinish}
                >
                    <Form.Item
                        name="_id"
                        hidden
                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>
            <ModalFaceback ref={modalFacebackRef} />
        </>
    );

})
    ;

export default PageChild;
