import { Button, Form, Input, InputRef, Modal, Tabs, TabsProps, Tooltip } from "antd";

import React, { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { emailLogin, sendEmailCode } from "@/api/modules/gpt";
import useLocalStore from "@/store/local";
import CountDown from "@/components/CountDown";

interface Props {
    onLogin?: () => void
}
const PageChild = forwardRef((props: Props, ref) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [form] = Form.useForm();
    const { token, setToken } = useLocalStore();
    const [isFinished, setIsFinished] = useState(true)

    const inputRef = useRef<InputRef | null>(null);
    const countRef: any = useRef();
    useImperativeHandle(ref, () => ({
        setIsModalOpen,
        onLogin: props.onLogin
    }));
    const onEmailFinish = () => {
        emailLogin(form.getFieldValue("email"), form.getFieldValue("code")).then(res => {
            setIsModalOpen(false)
            console.log(res.data);
            setToken(res.data.token)
            setTimeout(() => {
                props.onLogin?.()
            });
        })
        console.log("chengg");

    }
    const sendCode = async () => {
        await form.validateFields(["email"])
        countRef.current?.start();
        await sendEmailCode(form.getFieldValue("email"))
        setIsFinished(false)
        inputRef.current?.focus()
    }
    const items: TabsProps['items'] = [
        {
            key: '1',
            label: '邮箱登录',
            children: <Form
                form={form}
                onFinish={onEmailFinish}
            >
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: '邮箱不能为空' }, { type: "email", message: '邮箱格式错误' }]}
                >
                    <Input placeholder="请输入邮箱" allowClear />
                </Form.Item>

                <Form.Item
                    name="code"
                    rules={[{ required: true, message: '请输入邮箱验证码' }]}
                >
                    <Input
                        placeholder="请输入邮箱验证码"
                        maxLength={6}
                        allowClear
                        ref={inputRef}
                        addonAfter={<Button disabled={!isFinished} type="text"><span onClick={sendCode}>发送</span><CountDown onFinish={() => setIsFinished(true)} ref={countRef} time={Date.now() + 60 * 1000} format="(ss)s" /></Button>}
                    />
                </Form.Item>

                <Form.Item className="flex-center">
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>,
        },
        {
            key: '2',
            label: '密码登录',
            children: 'Content of Tab Pane 2',
        },
    ];
    return (
        < >
            <Modal wrapClassName="only_gpt" footer={null} closable={false} keyboard={false} title="请先完成登录操作" open={isModalOpen}>
                <Tabs defaultActiveKey="1" items={items} />
            </Modal>
        </>
    );

});

export default PageChild;
