import { Avatar, Button, Input, Modal, message } from "antd";
import { CopyOutlined, DownOutlined, EditOutlined, PauseCircleOutlined, RedoOutlined, ShareAltOutlined, SyncOutlined } from "@ant-design/icons";
import React, { FC, forwardRef, useEffect, useImperativeHandle, memo, useState, useMemo } from "react";
import { MarkdownText } from "@/components/MarkdownText";
import EditInput from "./EditInput";
import { Gpt, Pager } from "@/api/interface";
import DotLoading from "@/components/DotLoading";

interface Props {
    chatMessage: { list: Gpt.Message[] } & Pager;
    curIndex: number;
    isOnInput: boolean;
    onEditMsg?: (content: string) => void;
    onRetryMsg?: () => void;
}
const PageChild: FC<Props> = ({ curIndex, chatMessage, isOnInput = false, onEditMsg, onRetryMsg }, ref) => {
    const onCopy = (content: string) => {
        navigator.clipboard.writeText(content).then(() => {
            message.success("复制成功")
        })
    }
    const onEditMsgSubmit = (content: string, item: Gpt.Message) => {
        item.content = content;
        onEditMsg?.(content)
    }
    const onReadRevocation = () => {
        Modal.success({
            content: <MarkdownText text={item.content} />,
        });
    }

    const item = useMemo(() => chatMessage.list[curIndex], [chatMessage])
    const isEdit = useMemo(() => curIndex == (chatMessage.list.length - 2) && (item.role === "user"), [chatMessage.list.length])
    const isRetry = useMemo(() => curIndex === chatMessage.list.length - 1 && (item.role === "assistant"), [chatMessage.list.length])

    return (
        < >
            {item.is_revocation
                ? <div className="revocation">xtigher消息一条消息,<Button type="link" onClick={onReadRevocation}>点击查看</Button></div>
                : <><Avatar src={item.avatar} />
                    <div style={{
                        maxWidth: " calc(100% - 40px)",
                        marginBottom: " 40px"
                    }}>
                        <div className={`content ${item.role == "user" ? "content_user" : "content_other"}`}>
                            {(isOnInput && (curIndex == chatMessage.list.length - 1)) ? <DotLoading /> : <MarkdownText text={item.content} />}
                            {!(isOnInput || item.prompt) && <div className="tools flex-start">
                                <div className="tool_item" onClick={() => onCopy(item.content)}><CopyOutlined /><span>复制</span></div>
                                {chatMessage.total > 1 && <>
                                    <div className="tool_item"><ShareAltOutlined /><span>分享</span></div>
                                    {isEdit && <EditInput onEditMsg={content => onEditMsgSubmit(content, item)} content={item.content} />}
                                    {isRetry && <div className="tool_item" onClick={onRetryMsg}><RedoOutlined /><span>重新回答</span></div>}</>}
                            </div>}
                        </div>
                        {item.prompt ? (item.prompt.type === "image" ? <img style={{ maxWidth: "50px" }} src={item.prompt.content} /> : <a href={item.prompt.content}>文件</a>) : null}
                    </div>
                </>}



        </>
    );
}

export default PageChild;
