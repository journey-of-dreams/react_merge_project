import { Button, Tooltip } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import Sidebar from "@/components/Sidebar";

const PageChild = () => {
    return (
        < >
            <Sidebar title="智能体广场" className="agent_wrap">
                你好
                <div className="plusIntelige">
                    <Tooltip title="添加我的智能体">
                        <Button  shape="circle" icon={<PlusOutlined />} />
                    </Tooltip>
                </div>
            </Sidebar>
        </>
    );
};

export default PageChild;
