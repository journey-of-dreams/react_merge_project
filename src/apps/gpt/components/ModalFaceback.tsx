import { Input, message, Modal, } from "antd";

import React, { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { addFaceback, emailLogin, sendEmailCode } from "@/api/modules/gpt";

const prefix = "https://bucket-chabaidao-1308160773.cos.ap-guangzhou.myqcloud.com/img"
const faceList = [{ normal: prefix + "/1721015030312/.png", active: prefix + "/1721015287728/.png" },
{ normal: prefix + "/1721024626986/.png", active: prefix + "/1721024626620/.png" },
{ normal: prefix + "/1721024627064/.png", active: prefix + "/1721024627350/.png" },
{ normal: prefix + "/1721024627165/.png", active: prefix + "/1721024627350/.png" },
{ normal: prefix + "/1721024627241/.png", active: prefix + "/1721024627427/.png" }]

const PageChild = forwardRef<ModalFacebackRef>((props, ref) => {
    const [modalOpen, setModalOpen] = useState(false);
    const [faceForm, setFaceForm] = useState({ star: 0, content: "" })
    const submit = async () => {
        await addFaceback(faceForm)
        message.success("感谢您的反馈")
        setModalOpen(false)
    }

    useImperativeHandle(ref, () => ({ setModalOpen }))
    return (
        < >
            <Modal wrapClassName="faceback_modal" title={`用户反馈`} open={modalOpen} onOk={submit} onCancel={() => setModalOpen(false)} cancelText="以后再说" okText="确认">
                <div className="item">
                    <p className="title">你和xtiger的对话还愉快吗？</p>
                    {/* @ts-ignore */}
                    <div className="face_list flex-between">
                        {faceList.map((item, index) =>
                            <div key={index} className="face_item"
                                style={{ background: (faceForm.star - 1) === index ? "#2e67fa" : "#f3f5fc" }}
                                onClick={() => setFaceForm({ ...faceForm, star: index + 1 })} >
                                <div className="logo">
                                    <img className="img_1" src={(faceForm.star-1) === index ? item.active : item.normal} alt="" />
                                    <img className="img_2 position-center" src={item.active} alt="" />
                                </div>
                            </div>)}
                    </div>
                </div>
                <div className="item">
                    <p className="title">如果你愿意的话，欢迎说说你的想法，xtiger会努力做更好！</p>
                    <Input.TextArea value={faceForm.content} placeholder="欢迎说说你的想法" onChange={e => setFaceForm({ ...faceForm, content: e.target.value })} />
                </div>
            </Modal>
        </>
    );

});

export default PageChild;
