import { Button, Input, Tooltip, Upload, UploadFile, UploadProps, message } from "antd";
import { CloseCircleFilled, DownOutlined, EditOutlined, FileAddOutlined, ImportOutlined, SendOutlined, UploadOutlined } from "@ant-design/icons";
import React, { FC, forwardRef, useEffect, useImperativeHandle, memo, useState } from "react";
import useLocalStore from "@/store/local";
import { upload } from "@/api/modules/gpt";
interface Props {
    isOnInput: boolean
    onSend?: (content: string, file?: { url: string, key: string, fileType: string }) => void;
}
interface File {
    key: string;
    url: string;
    fileType: string;
}

const PageChild: FC<Props> = (props, ref) => {
    const [value, setValue] = useState("")
    const [fileList, setFileList] = useState<UploadFile[]>([])
    const [loading, setLoading] = useState(false)
    const [file, setFile] = useState<File>()

    const send = () => {
        if (props.isOnInput) {
            return
        }
        props.onSend?.(value, file)
        setValue("")
        setFileList([])
    }

    const uploadProps: UploadProps = {
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                setLoading(false)
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        beforeUpload: (file) => {
            if (file.size > 10 * 1024 * 1024) {
                message.error("文件大小不能超过10M")
                return false;
            }
            const formdata = new FormData();
            formdata.append("file", file);
            setLoading(true);
            upload(formdata).then(res => {
                setFile(res.data)
                setLoading(false)
            });
            console.log(file);
            setFileList([file]);
            return false;
        },
        fileList,
        maxCount: 1,
        showUploadList: false
    };
    const delFile = () => {
        setFile(undefined)
        setFileList([])
    }
    return (
        <>
            <div className="preview_file">
                {fileList[0]?.type ? <div className="preview_item">{fileList[0].type.includes("image") ? <div className="img_wrap"><img src={file?.url} /></div> : <div>file</div>} <CloseCircleFilled onClick={delFile} className="clear_icon" /></div> : null}
            </div>
            <div className="input_wrap">

                <div className="icon_list flex-start">
                    <Tooltip title={<><p>一次最多上传1个文件</p><p>格式Word/PDF/TXT，单个最大10MB</p></>}>
                        <Upload {...uploadProps} accept=".txt, .pdf, .doc, .docx">
                            <span className="icon_item"><UploadOutlined />文件</span>
                        </Upload>
                    </Tooltip>
                    <Tooltip title={<><p>当前支持上传1张图片</p><p>n最大10MB</p></>}>
                        <Upload {...uploadProps} accept=".png, .jpg, .jpeg, .webp, .gif">
                            <span className="icon_item"><FileAddOutlined />图片</span>
                        </Upload>
                    </Tooltip>
                    <Tooltip title={<><p>一键调用指令</p><p>快捷键：/</p>  </>}>
                        <span className="icon_item"><ImportOutlined />指令</span>
                    </Tooltip>


                </div>

                <Input.TextArea
                    placeholder="您想问什么呢"
                    autoSize={{ minRows: 2, maxRows: 6 }}
                    autoFocus
                    onChange={(e) => setValue(e.target.value)}
                    value={value}
                    onPressEnter={send}
                    onFocus={e => setValue(e.target.value.trim())}
                />
                <div className="send_btn">
                    <Button disabled={props.isOnInput || !value.trim()} icon={<SendOutlined />} onClick={send} type="primary">发送</Button>
                </div>

            </div>
        </>


    );
}

export default PageChild;
