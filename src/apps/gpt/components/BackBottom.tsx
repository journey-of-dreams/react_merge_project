import { Button } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { FC, forwardRef } from "react";

interface Props {
    isShow: boolean;
    onClick?: () => void;
}
const PageChild: FC<Props> = forwardRef((props, ref) => {
    return (
        < >
            <Button className={`back_bottom ${props.isShow ? 'show' : 'hidden'}`} icon={<DownOutlined />} onClick={props.onClick} />
        </>
    );
})

export default PageChild;
