import React, { Suspense, useEffect } from "react";
import { Outlet } from "react-router-dom";
import { ConfigProvider } from "antd-mobile";
import enUS from "antd-mobile/es/locales/en-US";
import qs from "qs";
import { setLang } from "./lang";
import { langTransform } from "./utils";
const query: Record<string, any> = qs.parse(location.search.slice(1));
const App: React.FC = () => {
  langTransform(query.lan)

  return (
    <ConfigProvider locale={enUS}>
      <Suspense fallback={"Loading..."}>
        <Outlet />
      </Suspense>
    </ConfigProvider>
  );
};
export default App;
