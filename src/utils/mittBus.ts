import mitt from "mitt";

const mittBus = mitt<any>();

export default mittBus;
