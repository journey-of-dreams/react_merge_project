import { setLang } from "@/lang";

/**
 * @description 时间戳转指定时间格式(当前日期)
 * @param timestamp 时间戳(支持10位/13位)
 * @param format 格式化字符串(yyyy-MM-dd HH:mm:ss)
 * @return void 格式化后的字符串
 * */
export function formatDate(timestamp?: number, format = "MM/dd/yyyy"): string {
  let date: Date;
  if (timestamp) {
    if ((timestamp + "").length === 13) {
      date = new Date(timestamp);
    } else if ((timestamp + "").length === 10) {
      date = new Date(timestamp * 1000);
    } else {
      date = new Date(timestamp);
    }
  } else {
    date = new Date();
  }
  const o = {
    "M+": date.getMonth() + 1, // 月份
    "d+": date.getDate(), // 日
    "h+": date.getHours() % 12 === 0 ? 12 : date.getHours() % 12, // 小时
    "H+": date.getHours(), // 小时
    "m+": date.getMinutes(), // 分
    "s+": date.getSeconds(), // 秒
    "q+": Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds(), // 毫秒
    a: date.getHours() < 12 ? "上午" : "下午", // 上午/下午
    A: date.getHours() < 12 ? "AM" : "PM", // AM/PM
  } as any;
  console.log(date, date.getHours());

  if (/(y+)/.test(format)) {
    format = format.replace(
      RegExp.$1,
      (date.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  }
  for (let k in o) {
    if (new RegExp("(" + k + ")").test(format)) {
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
      );
    }
  }
  return format;
}

/**
 *
 * @description 根据渠道名获取渠道值
 * @param channel 渠道
 * @returns number
 */
export function getChannel(channel: string) {
  switch (channel) {
    case "ThinkCar":
      return 1;
    case "MuCar":
      return 2;
    case "ThinkDiag":
      return 3;
    case "ThinkCar Overseas Offline":
      return 4;
    case "	ThinkCar USA":
      return 5;
    case "ThinkCarPro":
      return 6;
    case "ThinkMini":
      return 7;
    case "thinkDriver":
      return 8;
    case "ICarScan":
      return 9;
    case "MyThinkCar":
      return 10;
    case "TopDon":
      return 11;
    case "MUCAR VO6":
      return 12;
    case "MUCAR CS":
      return 13;
    case "THINKTOOL PD8":
      return 14;
    case "MUCAR CDE900":
      return 15;
    case "ThinkBay":
      return 16;
    case "	THINKCAR TPMS 900":
      return 17;
    case "MUCAR VO7":
      return 18;
    case "Thinkcar Ma":
      return 19;
    case "Cde 900 Pro":
      return 20;
    case "Mucar Vo8":
      return 21;
    case "X10_PRO_US":
      return 22;
    case "TOOL_EVD_PRO":
      return 23;
    case "Smart DIag mini":
      return 24;
    case "Ediag":
      return 25;
    default:
      return "";
  }
}
/**
 * @description 获取支持的语言列表
 * @returns {string[]}
 */
export function supportLanguage() {
  return Object.keys(import.meta.glob("../lang/lan/*.json")).map((key) =>
    key.slice(12, -5)
  );
}
/**
 * @description 语言转换
 * @param lan 语言
 * @returns void
 */
export function langTransform(lan: string) {
  switch (lan) {
    case "en-us":
      lan = "en";
      break;
    case "cn":
      lan = "zh";
      break;
    case "es":
      lan = "spa";
      break;
    case "fr":
      lan = "fra";
      break;
    case "hk":
      lan = "cht";
      break;
    case "ja":
      lan = "jp";
      break;
    case "ko":
      lan = "kor";
      break;
    default:
      if (!supportLanguage().includes(lan)) {
        lan = "en";
      }
      break;
  }
  setLang(lan);
}
export function isPc() {
  return window.innerWidth > 768;
}

/**
 * @description 函数防抖
 * @callback fn 需要包装的函数
 * @return
 * @param delay 延迟时间，单位ms(200)
 */
export function debounce(
  fn: { apply: (arg0: any, arg1: any) => void },
  delay = 200
) {
  let timeId: any = null;
  return function (this: any, ...args: any) {
    if (timeId) {
      clearTimeout(timeId);
    }
    timeId = setTimeout(() => {
      timeId = null;
      fn.apply(this, args);
    }, delay);
  };
}

/**
 * @description 函数节流
 * @callback fn 需要包装的函数
 * @return
 * @param time 节流时间，单位ms(200)
 * @param immediate 是否立即执行(false)
 */
export function throttle(callback: Function, delay = 100) {
  let isThrottled = false;
  return function (...args: any) {
    if (!isThrottled) {
      callback.apply(this, args);
      isThrottled = true;
      setTimeout(() => (isThrottled = false), delay);
    }
  };
};