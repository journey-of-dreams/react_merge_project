import axios, {
  AxiosInstance,
  AxiosError,
  AxiosRequestConfig,
  AxiosResponse,
} from "axios";
import {
  showFullScreenLoading,
  tryHideFullScreenLoading,
} from "./helper/serviceLoading";
import { ResultData } from "@/api/interface";
import { checkStatus } from "./helper/checkStatus";
// import useStore from "@/store";
import { Toast } from "antd-mobile";
import localStore from "@/store/local";
import qs from "qs";
const config = {
  // 默认地址请求地址，可在 .env 开头文件中修改
  baseURL: "/",
  // baseURL: import.meta.env.VITE_API_URL as string,
  // 设置超时时间（10s）
  timeout: 20000,
  // 跨域时候允许携带凭证
  // withCredentials: true,
};
// const store = useStore();

class RequestHttp {
  service: AxiosInstance;
  public constructor(config: AxiosRequestConfig) {
    // 实例化axios
    this.service = axios.create(config);

    /**
     * @description 请求拦截器
     * 客户端发送请求 -> [请求拦截器] -> 服务器
     * token校验(JWT) : 接受服务器返回的token,存储到vuex/pinia/本地储存当中
     */
    this.service.interceptors.request.use(
      (config: any) => {
        const query: Record<string, any> = qs.parse(location.search.slice(1));

        // * 如果当前请求不需要显示 loading,在 api 服务中通过指定的第三个参数: { headers: { noLoading: true } }来控制不显示loading，参见loginApi
        let token = localStore.getState().token;

        if (query.token) {
          token = query.token;
          localStore.setState({ token: token! });
        }
        config.headers!.noLoading || showFullScreenLoading();
        return {
          ...config,
          headers: { ...config.headers,  token },
        };
      },
      (error: AxiosError) => {
        return Promise.reject(error);
      }
    );

    /**
     * @description 响应拦截器
     *  服务器换返回信息 -> [拦截统一处理] -> 客户端JS获取到信息
     */
    this.service.interceptors.response.use(
      (response: AxiosResponse) => {
        const { data, request, headers } = response;
        // * 在请求结束后，并关闭请求 loading
        tryHideFullScreenLoading();

        // * 全局错误信息拦截（防止下载文件得时候返回数据流，没有code，直接报错）
        // if (data.code !== 0 && data.code !== 200) {
        // 	Toast.show({
        // 		icon: 'fail',
        // 		content: data.message,
        // 	})
        // 	return Promise.reject(data);
        // }
        // * 成功请求（在页面上除非特殊情况，否则不用处理失败逻辑）
        console.log(data.code);

        if (!(data.code == 0 || data.code == 200 || data.success)) {
          Toast.show(data.message);
          return Promise.reject(data.message);
        } else {
          return data;
        }
      },
      async (error: AxiosError) => {
        const { response } = error;
        tryHideFullScreenLoading();
        // 请求超时单独判断，因为请求超时没有 response
        if (error.message.indexOf("timeout") !== -1)
          Toast.show({
            icon: "fail",
            content: "请求超时！请您稍后重试",
          });
        // 根据响应的错误状态码，做不同的处理
        if (response) checkStatus(response.status);
        return Promise.reject(error);
      }
    );
  }

  // * 常用请求方法封装
  get<T>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
    return this.service.get(url, { params, ..._object });
  }
  post<T>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
    return this.service.post(url, params, _object);
  }
  put<T>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
    return this.service.put(url, params, _object);
  }
  patch<T>(url: string, params?: object, _object = {}): Promise<ResultData<T>> {
    return this.service.patch(url, params, _object);
  }
  delete<T>(url: string, params?: any, _object = {}): Promise<ResultData<T>> {
    return this.service.delete(url, { params, ..._object });
  }
}

export default new RequestHttp(config);
