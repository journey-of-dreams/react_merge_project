// * 请求响应参数(不包含data)
export interface Result {
  code: string;
  message: string;
}
export interface Pager {
  page: number;
  size: number;
  total: number;
}
// * 请求响应参数(包含data)
export interface ResultData<T> {
  time: number;
  data: T;
  result: T;
  code: number;
  message: string;
}

// * 分页响应参数
export interface ResPage<T> {
  datalist: T[];
  page: number;
  size: number;
  total: number;
}

export namespace VipPay {
  export interface GetAddress {
    id?: string;
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
    country: string;
    province: string;
    city: string;
    zip_code: string;
    address: string;
    address_two: string;
    state: 0 | 1;
  }
  export interface GetorderInfo {
    shipping?: string;
    shipping_id?: number;
    mailing_id?: number;
    order_goods: { good_id?: number; number: number; sku?: string }[];
  }
  export interface CreateOrder {
    appid?: string;
    channel?: number | string;
    channel_type?: number;
    pay_type?: number;
    shipping_id?: number;
    device_sn?: string;
    mailing_id?: number;
    mailing_information: { email?: string; is_security?: boolean }[];
    good_type: number;
    pay_price: string;
    original_price: string;
    order_goods: {
      good_id?: number;
      sku?: string;
      number: number;
      soft_package_id?: string;
      soft_name?: string;
      vin?: string;
      car_year?: string;
      car_model?: string;
      car_make?: string;
    }[];
  }
  export interface Login {
    user_name: string;
    password: string;
    verifiable_type: 1 | 2 | 3;
  }
  export interface SendCode {
    email: string;
    type: number;
  }

  export interface Register {
    email: string;
    verifiable_code: string;
    password?: string;
  }
  export interface VertifyCode {
    user_name: string;
    verifiable_code: string;
    type: number;
  }
}

export namespace Gpt {
  export interface UserInfo {
    email: string;
    name: string;
    avatar: string;
    last_dialog_id: string;
    _id: string;
  }
  export interface DialogItem {
    updated_date: string;
    title: string;
    _id: string;
  }
  export interface DialogList {
    list: DialogItem[];
    page: number;
    size: number;
    total: number;
  }
  export interface Message {
    is_revocation?: boolean;
    content: string;
    prompt?: {
      type: string,
      content: string
    };
    role: "assistant" | "user";
    date: number;
    avatar: string;
    [key: string]: any;
  }

  export interface Chat {
    message?: string;
    file?: FormData;
    dialog_id: string;
    type?: number;
  }
  export interface GetChatList {
    id: string;
    page?: number;
    size?: number;
  }
  export interface MessageEdit {
    content: string;
    dialog_id: string;
    signal: AbortSignal;
  }

  export interface AddFaceback {
    star: number;
    content?: string;
  }
}
