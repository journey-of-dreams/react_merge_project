import { PORT2,PORT5 } from "@/api/config/servicePort";
import http from "@/api";
/**
 * @name 获取诊断三合一报告
 */
export const diagMergeReport = (id: string) => {
	return http.get(`${PORT2}/Home/EnergyRecords/merge_report?id=${id}`);
};
/**
 * @name 新能源报告
 */
export const newEnergyReport = (detectId: string) => {
	return http.get(`${PORT5}/Home/energy_records/detectDetail`, { detectId });
};
