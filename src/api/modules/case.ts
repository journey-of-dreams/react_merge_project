import { PORT1 } from "@/api/config/servicePort";
import http from "@/api";
// import { Login, Chat } from "@/api/interface/index";

const cloud_api = "https://apicloud.mythinkcar.com/api";
/**
 * @name band查询
 */
export const brandList = () => {
  return http.get(
    `${cloud_api}/api/vehicleModelData/selectModelList?lang=EN&pdtType=1&channel=3&vehicleType=2&lan=EN&filterReset=1`
  );
};
/**
 * @name model/year查询
 */
export const modelYearList = (type: string, swId: string, model?: string) => {
  return http.get(`${cloud_api}/api/vehicleModelData/getModelSelect`, {
    swId,
    model,
    type,
    lang: "EN",
    lan: "EN",
  });
};
/**
 * @name 发送邮件
 */
export const sendEmail = (data: any) => {
  return http.post(`${PORT1}/api/SendCaseStudyEmail`, data);
};
/**
 * @name 文件上传
 */
export const publicUpload = (data: any) => {
  return http.post<{ url: string }>(`${PORT1}/api/publicUpload`, data, {
    headers: {
      contentType: "multipart/form-data",
    },
  });
};

/**
 * @name 获取产品列表
 */
export const getEurList = () => {
  return http.get(`${PORT1}/api/eur_product/get_list`);
};

/**
 * @name 获取产品说明详情
 */
export const getEurInfo = (prs: any) => {
  return http.get(`${PORT1}/api/eur_product/get_info`, prs);
};
