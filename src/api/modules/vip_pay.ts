import { PORT3, PORT4 } from "@/api/config/servicePort";
import http from "@/api";
import { Result, VipPay } from "../interface";
/**
 * @name 获取会员支付列表
 */
export const getBuyMemberList = (lang: string, device_sn: string) => {
    return http.get<MemberList>(`${PORT3}/api/v2/member/getBuyMemberList`, { device_sn, is_open: 1 }, { headers: { lang } });
};
/**
 * @name 获取国家下拉列表
 */
export const getNationList = () => {
    return http.get<NationList[]>(`${PORT3}/api/getCountryList`);
};
/**
 * @name 获取州下拉列表
 */
export const getStateList = (country_id: number) => {
    return http.get<string[]>(`${PORT3}/api/getStateList`, { country_id });
};
/**
 * @name 添加地址
 */
export const addAddress = (data: VipPay.GetAddress) => {
    return http.post<Result>(`${PORT3}/api/userAddress`, data);
};
/**
 * @name 获取地址
 */
export const getAddress = () => {
    return http.get<AddressInfo[]>(`${PORT3}/api/userAddress`);
};
/**
 * @name 更新地址
 */
export const updateAddress = (data: VipPay.GetAddress) => {
    return http.put<Result>(`${PORT3}/api/userAddress/${data.id}`, data);
};
/**
 * @name 删除地址
 */
export const delAddress = (id: string) => {
    return http.delete<Result>(`${PORT3}/api/userAddress/${id}`);
};
/**
 * @name 获取订单信息
 */
export const getorderInfo = (data: VipPay.GetorderInfo) => {
    return http.post<OrderParams>(`${PORT4}/api/v2/order/get_expense_info`, data);
};
/**
 * @name 创建订单
 */
export const createOrder = (data: VipPay.CreateOrder) => {
    return http.post<OrderParams>(`${PORT4}/api/v2/order/create_order`, data, { appid: data.appid });
};
/**
 * @name 获取开通记录
 */
export const getOrderRecord = (headsn: string) => {
    return http.get(`${PORT3}/api/member/getOrderRecord`, { headsn });
};
/**
 * @name 登录
 */
export const login = (data: VipPay.Login) => {
    return http.post<{ access_token: string }>(`${PORT3}/api/login`, data);
};
/**
 * @name 发送验证码
 */
export const sendCode = (data: VipPay.SendCode) => {
    return http.post<{ interval: number }>(`${PORT3}/api/send/emailCode`, data);
};
/**
 * @name 注册
 */
export const register = (data: VipPay.Register) => {
    return http.post<{ access_token: string }>(`${PORT3}/api/user/store`, data);
};
/**
 * @name 获取设备列表
 */
export const getDeviceList = () => {
    return http.get<{ list: DeviceInfo[] }>(`${PORT3}/api/device/list`, { is_tpms: 0 });
};
/**
 * @name 修改用户信息
 */
export const edituserInfo = (password: string) => {
    return http.post(`${PORT3}/api/user/set`, { password });
};

/**
 * @name 验证验证码
 */
export const vertifyCode = (data: VipPay.VertifyCode) => {
    return http.post(`${PORT3}/api/verify/code`, data);
};

