import http from "@/api";
import { Result, Gpt, Pager } from "../interface";
import localStore from "@/store/local";
import { PORT1 } from "../config/servicePort";

/**
 * @name 发送邮箱验证码
 */
export const sendEmailCode = (email: string) => {
  return http.post(`${PORT1}/trial/common/sendEmailCode`, { email });
};
/**
 * @name 邮箱登录
 */
export const emailLogin = (email: string, code: string) => {
  return http.post<{ token: string }>(
    `${PORT1}/trial/common/emailLogin`,
    {
      email,
      code,
    }
  );
};
/**
 * @name 获取用户信息
 */
export const getUserInfo = () => {
  return http.get<Gpt.UserInfo>(`${PORT1}/trial/gpt/getUserInfo`, {}, { headers: { noLoading: true }, });
};
/**
 * @name 更新用户信息
 */
export const updateUserInfo = (avatar: FormData) => {
  return http.patch<{ url: string }>(
    `${PORT1}/trial/gpt/updateUserInfo`,
    avatar,
    { headers: { noLoading: true } }
  );
};
/**
 * @name 获取会话列表
 */
export const getDialogList = () => {
  return http.get<Gpt.DialogList>(`${PORT1}/trial/gpt/getDialogList`, {}, { headers: { noLoading: true }, });
};
/**
 * @name 新建会话
 */
export const createDialog = (robot_id?: string) => {
  return http.post<{ message: Gpt.Message } & Gpt.DialogList>(
    `${PORT1}/trial/gpt/createDialog`,
    { robot_id },
    { headers: { noLoading: true }, }
  );
};
/**
 * @name 编辑会话
 */
export const editDialog = (id: string, title: string) => {
  return http.patch(
    `${PORT1}/trial/gpt/editDialog`,
    { id, title },
    { headers: { noLoading: true } }
  );
};
/**
 * @name 删除会话
 */
export const deleteDialog = (id: string) => {
  return http.delete(`${PORT1}/trial/gpt/deleteDialog/${id}`, {
    headers: { noLoading: true },
  });
};
/**
 * @name 聊天
 */
export const chat = (data: Gpt.Chat) => {
  return http.post(`${PORT1}/trial/gpt/chat`, data, {
    headers: { noLoading: true, "Content-Type": "application/json" },
  });
};
/**
 * @name fetch聊天
 */
export const fetchChat = (data: Gpt.Chat, file?: FormData) => {
  const headers = {
    token: localStore.getState().token,
  } as any;
  if (file) {
    file.append("message", data.message!);
    file.append("dialog_id", data.dialog_id);
    file.append("type", "" + data.type);
  } else {
    headers["Content-Type"] = "application/json";
  }
  console.log(file);

  return new Promise((resolve, reject) => {
    fetch(`${PORT1}/trial/gpt/chat`, {
      method: "POST",
      headers: headers,
      body: file
        ? file
        : JSON.stringify({
          message: data.message,
          dialog_id: data.dialog_id,
          type: data.type,
        }),
    })
      .then((res: Response) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
/**
 * @name 获取聊天记录
 */
export const getChatList = (params: Gpt.GetChatList) => {
  return http.get<{ list: Gpt.Message[] } & Pager>(
    `${PORT1}/trial/gpt/getDialogMessage`,
    params,
    { headers: { noLoading: true }, }
  );
};

/**
 * @name 提交用户反馈
 */

export const addFaceback = (data: Gpt.AddFaceback) => {
  return http.post(`${PORT1}/trial/gpt/addFaceback`, data);
};
/**
 * @name 文件上传
 */

export const upload = (data: FormData) => {
  return http.post<{ url: string, key: string, fileType: string }>(`${PORT1}/trial/common/upload`, data, { headers: { noLoading: true } })
};

