
const readline = require("readline");
const fs = require("fs");
const path = require("path");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
(async function () {
    rl.question("请输入项目名：(多级目录请用/分隔)\n", function (anwser) {
        const mdkr = `src/apps/${anwser}`;
        const isExist = fs.existsSync(mdkr);
        if (isExist) {
            console.log("当前目录已存在，不可创建！");
            rl.close();
        }
        fs.mkdirSync(mdkr, { recursive: true });
        fs.readdirSync("src/template").forEach(file => {
            const srcFile = path.join("src/template", file);
            const destFile = path.join(mdkr, file);
            fs.copyFileSync(srcFile, destFile)
        })
        let data = fs.readFileSync("src/config/index.js", 'utf8');
        data = data.trim().slice(0, data.lastIndexOf('\n')) + `\n  ${anwser.replaceAll("/","_")}: "./src/apps/${anwser}/main.tsx",\n};`;

        fs.writeFileSync("src/config/index.js", data);
        console.log("创建成功：\n" + mdkr)
        rl.close();
    })

    rl.on("close", () => {
        process.exit(0);
    })
})()





